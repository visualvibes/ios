//
//  ScanTableViewController.swift
//  Light Controller
//
//  Created by Josh Jordan on 5/27/17.
//  Copyright © 2017 Josh Jordan. All rights reserved.
//

import CoreBluetooth
import UIKit
class ScanTableViewController: BaseVC, CBCentralManagerDelegate, CBPeripheralDelegate {
    
    var peripherals = Array<CBPeripheral>()
    var peripheral:CBPeripheral!
    var manager:CBCentralManager!
    
    @IBOutlet weak var tblView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        print("Start")
        self.setNavigation()
        manager = CBCentralManager(delegate: self, queue: DispatchQueue.main)
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(pulldownRefresh), for: .valueChanged)
        self.tblView.refreshControl = refreshControl
        self.tblView.tableFooterView = UIView()
    }
    
    func setNavigation() {
        self.setNavigationBar(title: "BLE Device List", titleImage: nil, leftImage: nil, rightImage: nil, leftTitle: nil, rightTitle: nil, isLeft: false, isRight: false, bgColor: UIColor.systemBackground, textColor: UIColor.label, leftClick: { (btnLeft) in
            
        }) { (btnRight) in
            
        }
    }
    
    @objc func pulldownRefresh(refreshControl: UIRefreshControl) {
        peripherals.removeAll()
        manager.stopScan()
        manager.scanForPeripherals(withServices: nil, options: nil)
        refreshControl.endRefreshing()
    }

    
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        if central.state == CBManagerState.poweredOn {
            print("Scan start")
            central.scanForPeripherals(withServices: nil, options: nil)
        } else {
            print("Bluetooth not available.")
        }
    }
    
    func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {
            if peripheral.name != nil && !peripherals.contains(peripheral){
                peripherals.append(peripheral)
                print("peripheral " + peripheral.name!)
                self.tblView.reloadData()
            }
        }
    
    func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
        print("didConnectPeripheral")
        peripheral.discoverServices(nil)
    }
    
    func centralManager(_ central: CBCentralManager, didFailToConnect peripheral: CBPeripheral) {
        print("connect failed")
        //peripheral.discoverServices(nil)
    }

    func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?) {
        print("peripheral discover services")
        for service in peripheral.services! {
            _ = service as CBService
            print("service \(service.uuid.uuidString)")
//            if service.UUID == BEAN_SERVICE_UUID {
//                peripheral.discoverCharacteristics(nil, forService: thisService)
//            }
        }
        self.performSegue(withIdentifier: "deviceConnected", sender: self)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        print("segue \(segue.identifier!)")
        switch segue.identifier! {
        case "deviceConnected":
            let destination = segue.destination as! BleTabBarController
            destination.manager = manager
            destination.peripheral = peripheral
            break;
        default:
            break
        }
    }
}

extension ScanTableViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return peripherals.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "btScanCell", for: indexPath)
        let peripheral = peripherals[indexPath.section]
        cell.textLabel?.text = peripheral.name
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("clicked \(indexPath.section) \(peripherals[indexPath.section].name!)")
            self.manager.stopScan()
            AppPrefsManager.sharedInstance.removeDataFromPreference(key: AppPrefsManager.sharedInstance.SELECTEDPRESET)
            self.peripheral = peripherals[indexPath.section]
            self.peripheral.delegate = self
            manager.connect(self.peripheral, options: nil)
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 10
    }
    
}

