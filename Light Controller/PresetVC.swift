//
//  PresetVC.swift
//  Light Controller
//
//  Created by Darshan Gajera on 5/28/19.
//  Copyright © 2019 Josh Jordan. All rights reserved.
//

import UIKit
import SQLite3
import StepSlider
class PresetCell: KDRearrangeableCollectionViewCell {
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var btnMore: UIButton!
}
class PresetVC: BaseVC, UICollectionViewDelegateFlowLayout,KDRearrangeableCollectionViewDelegate, UIGestureRecognizerDelegate {

    var alertController: UIAlertController = UIAlertController()
    @IBOutlet weak var collectionViewRearrangeableLayout: KDRearrangeableCollectionViewFlowLayout!
    @IBOutlet weak var btnAddPreset: UIButton!
    @IBOutlet weak var clnView: UICollectionView!
    @IBOutlet weak var SliderOnOff: StepSlider!
    
    //MARK: Global Variables
    var tempPresetName: String? = nil
    let operationQ = OperationQueue.init()
    var intSelectedPresetIndex: Int = 0 // using in timer method
    var timer: Timer?
    var boolAllDataDeselect: Bool = false
    var arrPresetData: NSMutableArray = NSMutableArray()
    var arrSelectedData: NSMutableArray = NSMutableArray()
    var tabbar:BleTabBarController!
    var tmpCurrentPreset: Preset?
    //MARK: view Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.buttonAction()
        
//        print(AppPrefsManager.sharedInstance.getMainPreset()?.mid_animation_option)
//        print(AppPrefsManager.sharedInstance.getMainPreset()?.bass_animation_option)
//        print(AppPrefsManager.sharedInstance.getMainPreset()?.trebel_animation_option)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.setup()
        self.getPresetData()
        self.navigationController?.tabBarController?.tabBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        AppPrefsManager.sharedInstance.setValueChange(obj: false)
    }
    
    //MARK: Other Methods
    
    // button actions
    func buttonAction() {
        btnAddPreset.addAction {
            let strPName = self.generateNewPresetName()
            self.delayWithSeconds(0.2, completion: {
                let add = Storyboard.main.storyboard().instantiateViewController(withIdentifier: Identifier.AddPreset.rawValue) as! AddPresetVC
                add.presetName = strPName
                add.strTitle = "Add New Preset"
                self.navigationController?.pushViewController(add, animated: true)
            })
        }
    }
    
    // all setup
    func setup() {
//        self.collectionViewRearrangeableLayout.draggable = true
//        self.collectionViewRearrangeableLayout.axis = .free
        tabbar = self.tabBarController as? BleTabBarController
        self.setNavigation()
        
        // Slider Setup
        SliderOnOff.labels = ["OFF","ON"]
        SliderOnOff.tintColor = UIColor.lightGray
        self.SliderOnOff.labelOrientation = .up
        self.SliderOnOff.adjustLabel = false
        self.SliderOnOff.labelFont = UIFont.boldSystemFont(ofSize: 15.0)
        self.SliderOnOff.labelColor = UIColor.gray
        self.defaultSelected()
    }

    
    func defaultSelected() {
        self.clnView.reloadData()
        if tabbar.bassAnim == 20 && tabbar.trebleAnim == 20 && tabbar.midAnim == 20 && tabbar.bassAuto == 0 && tabbar.midAuto == 0 && tabbar.trebleAuto == 0 {
            SliderOnOff.sliderCircleImage =
                UIImage(named: "icoOff")
            SliderOnOff.index = 0
        } else {
            SliderOnOff.sliderCircleImage =
                UIImage(named: "icoOn")
            SliderOnOff.index = 1
        }
        
        SliderOnOff.addTarget(self, action: #selector(valueSliderChange), for: .valueChanged)
        if AppPrefsManager.sharedInstance.isKeyExistInPreference(key: AppPrefsManager.sharedInstance.VALUECHANGE) {
            if AppPrefsManager.sharedInstance.getValueChange() == true {
                self.arrSelectedData.removeAllObjects()
                AppPrefsManager.sharedInstance.removeDataFromPreference(key: AppPrefsManager.sharedInstance.SELECTEDPRESET)
            }
        }
    }
    
    // All Off
    func allDeselect() {
        arrSelectedData.removeAllObjects()
        clnView.reloadData()
    }
    
    // Timer Method
    @objc func repeatPreset() {
        self.intSelectedPresetIndex = self.intSelectedPresetIndex + 1
        
        if self.intSelectedPresetIndex > self.arrSelectedData.count - 1 {
           self.intSelectedPresetIndex = 0
        } else {
            
        }
        self.passAllValueToDevice()
    }
    
    // slider All on/off
   @objc func valueSliderChange() {
        if SliderOnOff.index == 0 {
            SliderOnOff.sliderCircleImage =
                UIImage(named: "icoOff")
            AppPrefsManager.sharedInstance.setValueChange(obj: false)
            if tmpCurrentPreset?.trebel_animation_option == 20 && tmpCurrentPreset?.mid_animation_option == 20 && tmpCurrentPreset?.bass_animation_option == 20 &&
                tmpCurrentPreset?.trebel_auto_animation == 0 && tmpCurrentPreset?.mid_auto_animation == 0 && tmpCurrentPreset?.bass_auto_animation == 0 {
            } else {
                self.allDeselect()
                AppPrefsManager.sharedInstance.removeDataFromPreference(key: AppPrefsManager.sharedInstance.SELECTEDPRESET)
                self.SliderOnOff.labelColor = UIColor.gray
                self.tabbar.bleFifo.append((3, Data([30, 0x00, UInt8(29)])))
                self.tabbar.nextBleMessage()
                self.tabbar.bleFifo.append((3, Data([10, 0x00, UInt8(29)])))
                self.tabbar.nextBleMessage()
                self.tabbar.bleFifo.append((3, Data([20, 0x00, UInt8(29)])))
                self.tabbar.nextBleMessage()
                tabbar.trebleAnim = 20
                tabbar.midAnim = 20
                tabbar.bassAnim = 20
                tabbar.midAuto = 0
                tabbar.bassAuto = 0
                tabbar.trebleAuto = 0
            }
        } else {
            boolAllDataDeselect = true
            SliderOnOff.sliderCircleImage =
                UIImage(named: "icoOn")
                var mainPreset: Preset = Preset()
                mainPreset = AppPrefsManager.sharedInstance.getMainPreset()!
//                print(AppPrefsManager.sharedInstance.getMainPreset()?.mid_animation_option)
//                print(AppPrefsManager.sharedInstance.getMainPreset()?.bass_animation_option)
//                print(AppPrefsManager.sharedInstance.getMainPreset()?.trebel_animation_option)
                let dicValue = ["selectedPreset":mainPreset]
                NotificationCenter.default.post(name: .changeControllerValue, object: self, userInfo: dicValue)
                self.passAllValueToDevice()
                clnView.reloadData()
        }
    }
    
    // Set Navigation Bar
    func setNavigation() {
        self.setNavigationBar(title: "Presets", titleImage: nil, leftImage: "\u{2039}".toUIImage(), rightImage: nil, leftTitle: nil, rightTitle: nil, isLeft: true, isRight: false, bgColor: UIColor.systemBackground, textColor: UIColor.label, leftClick: { (btnLeft) in
            self.timer?.invalidate()
            self.navigationController?.popViewController(animated: true)
        }) { (btnRight) in
            
        }
    }
    
    // Get all Preset Data
    func getPresetData() {
        self.arrPresetData = NSMutableArray()
        DBManager.shared.getAllPresetData(withID: 0) { (arrData) in
            if arrData != nil {
                self.arrPresetData = NSMutableArray(array: arrData!)
            }
        }
        arrSelectedData = NSMutableArray()
        if AppPrefsManager.sharedInstance.isKeyExistInPreference(key: AppPrefsManager.sharedInstance.SELECTEDPRESET) {
            let lastSelectedPreset = AppPrefsManager.sharedInstance.getSelectedPreset()
            let name = lastSelectedPreset?.preset_name
            for i in 0 ..< arrPresetData.count {
                let singlePreset = arrPresetData.object(at: i) as! Preset
                if singlePreset.preset_name ==  name {
                    arrSelectedData = NSMutableArray()
                    arrSelectedData.add(singlePreset.preset_name ?? "")
                }
            }
        }
        self.clnView.reloadData()
    }
    
    //MARK: Collection View Layout
    func collectionViewLayout() {
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.minimumInteritemSpacing = 15
        layout.minimumLineSpacing = 15
        clnView!.collectionViewLayout = layout
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var collectionViewSize = collectionView.frame.size
        collectionViewSize.width = (collectionViewSize.width/2.0)-8//Display Three elements in a row.
        collectionViewSize.height = 100
        return collectionViewSize
    }
    
    // MARK: Darg and Drop Method
    func moveDataItem(from source : IndexPath, to destination: IndexPath) -> Void {
        
        let item: Preset = arrPresetData.object(at: source.item) as! Preset
        arrPresetData.removeObject(at: source.item)
        arrPresetData.insert(item, at: destination.item)
        self.deleteAllData()
    }
}

//MARK: Collection View delegate
extension PresetVC: UICollectionViewDelegate, UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrPresetData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let preset = arrPresetData.object(at: indexPath.row) as! Preset
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PresetCell", for: indexPath) as! PresetCell
        cell.lblName.text = preset.preset_name ?? ""
        cell.tag = indexPath.item
        cell.btnMore.tag = indexPath.row
        cell.btnMore.addAction {
            self.presetAction(btn: cell.btnMore)
        }
        cell.btnMore.isEnabled = true
        if arrSelectedData.contains(preset.preset_name!) {
            cell.layer.borderWidth = 1.0
            cell.layer.borderColor = UIColor.green.cgColor
        } else {
            cell.layer.borderWidth = 1.0
            cell.layer.borderColor = UIColor.gray.cgColor
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print(indexPath.row)
        let cell = collectionView.cellForItem(at: indexPath) as! PresetCell
        cell.contentView.backgroundColor = UIColor.green
        self.delayWithSeconds(0.2) {
            cell.contentView.backgroundColor = UIColor.clear
        }
        self.delayWithSeconds(0.2) {
            let preset = self.arrPresetData.object(at: indexPath.row) as! Preset
            self.arrSelectedData.removeAllObjects()
            self.arrSelectedData.add(preset.preset_name!)
            AppPrefsManager.sharedInstance.setSelectedPreset(obj: preset.dictionary as AnyObject)
            AppPrefsManager.sharedInstance.setMainPreset(obj: preset.dictionary as AnyObject)
            self.SliderOnOff.index = 1
            self.valueSliderChange()
            AppPrefsManager.sharedInstance.setValueChange(obj: false)
            self.tmpCurrentPreset = preset
            let dicValue = ["selectedPreset":preset]
            NotificationCenter.default.post(name: .changeControllerValue, object: self, userInfo: dicValue)
            if preset.trebel_animation_option == 20 && preset.mid_animation_option == 20 && preset.bass_animation_option == 20 &&
                preset.trebel_auto_animation == 0 && preset.mid_auto_animation == 0 && preset.bass_auto_animation == 0 {
                self.SliderOnOff.index = 0
                self.valueSliderChange()
            }
            collectionView.reloadData()
        }
    }
    
        func passAllValueToDevice(){
                    var selectedPreset: Preset = Preset()
                    selectedPreset = AppPrefsManager.sharedInstance.getMainPreset()!
                            // Color Options
            
                            if selectedPreset.bass_color_options != nil {
                                self.tabbar.bleFifo.append((3, Data([17, 0x00, UInt8(selectedPreset.bass_color_options!)])))
                                self.tabbar.nextBleMessage()
                            }
                            if selectedPreset.bass_animation_option != nil {
                                self.tabbar.bleFifo.append((3, Data([10, 0x00, UInt8(selectedPreset.bass_animation_option! + 9)])))
                                self.tabbar.nextBleMessage()
                            }
                            if selectedPreset.bass_auto_animation != nil {
                                self.tabbar.bleFifo.append((3, Data([18, 0x00, UInt8(selectedPreset.bass_auto_animation!)])))
                                self.tabbar.nextBleMessage()
                                
                            }
                            if selectedPreset.bass_bright != nil {
                                self.tabbar.bleFifo.append((3, Data([12, 0x00, UInt8(selectedPreset.bass_bright!)])))
                                self.tabbar.trebleBrightness = Int(selectedPreset.bass_bright!)
                                self.tabbar.nextBleMessage()
                            }
                            if selectedPreset.bass_color != nil {
                                self.tabbar.bleFifo.append((3, Data([11, 0x00, UInt8(selectedPreset.bass_color!)])))
                                self.tabbar.trebleColor = Int(selectedPreset.bass_color!)
                                self.tabbar.nextBleMessage()
                            }
                            if selectedPreset.mid_color_options != nil {
                                self.tabbar.bleFifo.append((3, Data([27, 0x00, UInt8(selectedPreset.mid_color_options!)])))
                                self.tabbar.nextBleMessage()
                            }
                            if selectedPreset.mid_animation_option != nil {
                                self.tabbar.bleFifo.append((3, Data([20, 0x00, UInt8(selectedPreset.mid_animation_option! + 9)])))
                                self.tabbar.nextBleMessage()
                            }
                            if selectedPreset.mid_auto_animation != nil {
                                self.tabbar.bleFifo.append((3, Data([28, 0x00, UInt8(selectedPreset.mid_auto_animation!)])))
                                self.tabbar.nextBleMessage()
                            }
                            if selectedPreset.mid_bright != nil {
                                self.tabbar.bleFifo.append((3, Data([22, 0x00, UInt8(selectedPreset.mid_bright!)])))
                                self.tabbar.trebleBrightness = Int(selectedPreset.mid_bright!)
                                self.tabbar.nextBleMessage()
                            }
                            if selectedPreset.mid_color != nil {
                                self.tabbar.bleFifo.append((3, Data([21, 0x00, UInt8(selectedPreset.mid_color!)])))
                                self.tabbar.trebleColor = Int(selectedPreset.mid_color!)
                                self.tabbar.nextBleMessage()
                            }
                            if selectedPreset.trebel_color_options != nil {
                                self.tabbar.bleFifo.append((3, Data([37, 0x00, UInt8(selectedPreset.trebel_color_options!)])))
                                self.tabbar.nextBleMessage()
                            }
                            if selectedPreset.trebel_animation_option != nil {
                                self.tabbar.bleFifo.append((3, Data([30, 0x00, UInt8(selectedPreset.trebel_animation_option! + 9)])))
                                self.tabbar.nextBleMessage()
                            }
                            if selectedPreset.trebel_auto_animation != nil {
                                self.tabbar.bleFifo.append((3, Data([38, 0x00, UInt8(selectedPreset.trebel_auto_animation!)])))
                                self.tabbar.nextBleMessage()
                            }
                            if selectedPreset.trebel_bright != nil {
                                self.tabbar.bleFifo.append((3, Data([32, 0x00, UInt8(selectedPreset.trebel_bright!)])))
                                self.tabbar.trebleBrightness = Int(selectedPreset.trebel_bright!)
                                self.tabbar.nextBleMessage()
                            }
                            if selectedPreset.trebel_color != nil {
                                self.tabbar.bleFifo.append((3, Data([31, 0x00, UInt8(selectedPreset.trebel_color!)])))
                                self.tabbar.trebleColor = Int(selectedPreset.trebel_color!)
                                self.tabbar.nextBleMessage()
                            }
                    }
    }

//MARK: Databse operations functions.
extension PresetVC: UITextFieldDelegate {
    func presetAction(btn: UIButton){
        let preset = arrPresetData.object(at: btn.tag) as! Preset
        self.presentAlertWithTitle(presentStyle: .actionSheet, title: preset.preset_name!, message: "", options: "Edit Preset","Delete Preset","Rename Preset","Copy Preset","Cancel") { (index) in
            
            switch index{
            case 0:
                //edit
                let addpreset = Storyboard.main.storyboard().instantiateViewController(withIdentifier: Identifier.AddPreset.rawValue) as! AddPresetVC
                addpreset.strTitle = preset.preset_name
                addpreset.singlePreset = preset
                self.navigationController?.pushViewController(addpreset, animated: true)
                break
            case 1:
                //delete
                self.deletePreset(currentPreset: preset)
                break
            case 2:
                // rename
                self.renamePreset(currentPreset: preset)
                break
            case 3:
                // copy
                self.copyRecord(singlePreset: preset)
                break
            default:
                break
            }
        }
    }
    
    func copyRecord(singlePreset: Preset) {
        
        var nextPresetName: String?
        let arrPresetCopyNames = self.prepareNameArray(strName: singlePreset.preset_name!)
        let arrPresetNamesDB = DBManager.shared.getAllPresetName()
        
        for i in 0..<arrPresetCopyNames.count {
            if (arrPresetNamesDB.contains(arrPresetCopyNames.object(at: i))) {
            } else {
                nextPresetName = arrPresetCopyNames.object(at: i) as? String
                break
            }
        }
        
        let queryString = String(format: "INSERT INTO %@ (%@, %@, %@, %@ ,%@, %@, %@, %@, %@, %@, %@, %@ ,%@, %@, %@, %@) VALUES ('%@',%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d)", TableName.presetMaster, TableField.preset_name,TableField.bass_color, TableField.bass_bright, TableField.bass_color_options , TableField.bass_animation_option,TableField.bass_auto_animation , TableField.mid_color , TableField.mid_bright, TableField.mid_color_options , TableField.mid_animation_option ,TableField.mid_auto_animation , TableField.trebel_color , TableField.trebel_bright , TableField.trebel_color_options , TableField.trebel_animation_option ,TableField.trebel_auto_animation, nextPresetName!, singlePreset.bass_color!, singlePreset.bass_bright!, singlePreset.bass_color_options!, singlePreset.bass_animation_option!,singlePreset.bass_auto_animation! == 0 ? 0 : 1, singlePreset.mid_color!, singlePreset.mid_bright!, singlePreset.mid_color_options!, singlePreset.mid_animation_option!, singlePreset.mid_auto_animation! == 0 ? 0 : 1, singlePreset.trebel_color!, singlePreset.trebel_bright!, singlePreset.trebel_color_options!, singlePreset.trebel_animation_option!,singlePreset.trebel_auto_animation! == 0 ? 0 : 1)
        
        if DBManager.shared.insertRecord(strInsertQuery: queryString) {
            self.presentAlertWithTitle(presentStyle: .alert, title: GlobalConstants.APPNAME, message: SuccessMesssages.copy, options: "Ok") { (index) in
                if index == 0 {
                }
            }
            self.getPresetData()
        }
    }
    
    func prepareNameArray (strName: String) -> NSMutableArray {
        let arrNames: NSMutableArray = NSMutableArray()
        for i in 1..<100 {
            arrNames.add(String(format: "%@_%d", strName,i))
        }
        return arrNames
    }
    
    func deletePreset(currentPreset: Preset) {
        if AppPrefsManager.sharedInstance.isKeyExistInPreference(key: AppPrefsManager.sharedInstance.SELECTEDPRESET) {
            let selectedPreset = AppPrefsManager.sharedInstance.getSelectedPreset()!
            if currentPreset.preset_name == selectedPreset.preset_name {
                self.presentAlertWithTitle(presentStyle: .alert, title: "Delete Preset", message: "You can't delete preset as it currently selected", options: "Ok") { (index) in
                }
            } else {
                self.presentAlertWithTitle(presentStyle: .alert, title: "Delete Preset", message: "Are you sure want to delete \(currentPreset.preset_name!)?", options: "Yes","No") { (index) in
                    if index == 0 {
                        if DBManager.shared.executeQuery(strQuery: "DELETE FROM \(TableName.presetMaster) WHERE \(TableField.id) = ?", values: [currentPreset.id!]) {
                            self.presentAlertWithTitle(presentStyle: .alert, title: GlobalConstants.APPNAME, message: SuccessMesssages.delete, options: "Ok") { (index) in
                                if index == 0 {
                                }
                            }
                            self.getPresetData()
                        }
                    } else {
                        
                    }
                }
            }
        } else {
            self.presentAlertWithTitle(presentStyle: .alert, title: "Delete Preset", message: "Are you sure want to delete \(currentPreset.preset_name!)?", options: "Yes","No") { (index) in
                if index == 0 {
                    if DBManager.shared.executeQuery(strQuery: "DELETE FROM \(TableName.presetMaster) WHERE \(TableField.id) = ?", values: [currentPreset.id!]) {
                        self.presentAlertWithTitle(presentStyle: .alert, title: GlobalConstants.APPNAME, message: SuccessMesssages.delete, options: "Ok") { (index) in
                            if index == 0 {
                            }
                        }
                        self.getPresetData()
                    }
                } else {
                    
                }
            }
        }
    }
    
    //MARK: Textfield's delegate
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let set = NSCharacterSet(charactersIn: "ABCDEFGHIJKLMONPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_: !@#$%&*()'").inverted
        return string.rangeOfCharacter(from: set) == nil
    }
    
    func renamePreset(currentPreset: Preset) {
        tmpCurrentPreset = currentPreset
        self.alertController = UIAlertController(title: "Rename Preset", message: "", preferredStyle: .alert)
        self.alertController.addTextField { (txtPresetName) in
            txtPresetName.addTarget(self, action: #selector(self.renameAlertValueChange(txtF:)), for: .editingChanged)
            txtPresetName.placeholder = "Enter Preset Name"
            self.tempPresetName = currentPreset.preset_name!
            txtPresetName.text = currentPreset.preset_name!
            txtPresetName.keyboardType = .alphabet
            txtPresetName.autocapitalizationType = .words
            txtPresetName.delegate = self
        }
        
        let saveAction = UIAlertAction(title: "Update", style: .default, handler: { alert -> Void in
            let firstTextField = self.alertController.textFields![0] as UITextField
            if firstTextField.text!.count > 0 {
                if self.tempPresetName == firstTextField.text {
                    
                } else {
                    self.updateNameToDB(strPresetName: firstTextField.text, preset: currentPreset)
                }
            } else {
                self.presentAlertWithTitle(presentStyle: .alert, title: GlobalConstants.APPNAME, message: ErrorMesssages.enterPresetName, options: "Ok") { (index) in
                    
                }
            }
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: {
            (action : UIAlertAction!) -> Void in })
        self.alertController.addAction(saveAction)
        self.alertController.addAction(cancelAction)
        self.present(self.alertController, animated: true, completion: nil)
    }
    
    @objc func renameAlertValueChange(txtF: UITextField){
        if txtF.text!.count <= 0 {
            alertController.actions[0].isEnabled = false
        } else {
            if self.tempPresetName == txtF.text! {
                alertController.actions[0].isEnabled = true
            } else {
                if self.nameAlreadySaved(strName: txtF.text!, pid: (tmpCurrentPreset?.id!)!)! {
                    alertController.actions[0].isEnabled = true
                } else {
                    alertController.actions[0].isEnabled = false
                }
            }
        }
    }
    
    func nameAlreadySaved(strName: String, pid: Int)-> Bool? {
        if DBManager.shared.nameAlreadyExist(strQuery: "SELECT * FROM \(TableName.presetMaster) WHERE \(TableField.preset_name) = ?", values: [strName]) {
            return false
        } else {
            return true
        }
    }
    
    func updateNameToDB(strPresetName: String?, preset: Preset) {
        if DBManager.shared.executeQuery(strQuery: "UPDATE \(TableName.presetMaster) SET \(TableField.preset_name) = ? WHERE \(TableField.id) = ?", values: [strPresetName!,preset.id ?? 0]) {
            if AppPrefsManager.sharedInstance.getSelectedPreset()?.preset_name == preset.preset_name {
                var renamedPreset = AppPrefsManager.sharedInstance.getSelectedPreset()
                renamedPreset?.preset_name = strPresetName
                AppPrefsManager.sharedInstance.setSelectedPreset(obj: renamedPreset?.dictionary as AnyObject)
            }
            self.presentAlertWithTitle(presentStyle: .alert, title: GlobalConstants.APPNAME, message: SuccessMesssages.rename, options: "Ok") { (index) in
                if index == 0 {
                    self.getPresetData()
                }
            }
        }
    }
    
    //MARK: DeleteAll Data
    func deleteAllData() {
        if DBManager.shared.deleteAllPreset() {
            self.allAdd()
        }
    }
    
    //MARK:  Add all Data
    // after delete all data
    func allAdd() {
        for i in 0 ..< (arrPresetData.count) {
            let singlePreset = arrPresetData.object(at: i) as! Preset
            let queryString = String(format: "INSERT INTO %@ (%@, %@, %@, %@ ,%@, %@, %@, %@, %@, %@, %@, %@ ,%@, %@, %@, %@) VALUES ('%@',%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d)", TableName.presetMaster, TableField.preset_name,TableField.bass_color, TableField.bass_bright, TableField.bass_color_options , TableField.bass_animation_option,TableField.bass_auto_animation , TableField.mid_color , TableField.mid_bright, TableField.mid_color_options , TableField.mid_animation_option ,TableField.mid_auto_animation , TableField.trebel_color , TableField.trebel_bright , TableField.trebel_color_options , TableField.trebel_animation_option ,TableField.trebel_auto_animation,singlePreset.preset_name!,singlePreset.bass_color!,singlePreset.bass_bright!,singlePreset.bass_color_options!,singlePreset.bass_animation_option!,singlePreset.bass_auto_animation! == 0 ? 0 : 1,singlePreset.mid_color!,singlePreset.mid_bright!,singlePreset.mid_color_options!,singlePreset.mid_animation_option!,singlePreset.mid_auto_animation! == 0 ? 0 : 1,singlePreset.trebel_color!,singlePreset.trebel_bright!,singlePreset.trebel_color_options!,singlePreset.trebel_animation_option!,singlePreset.trebel_auto_animation! == 0 ? 0 : 1)
            
            if DBManager.shared.insertRecord(strInsertQuery: queryString) {
                print("preset saved successfully")
            }
        }
            self.getPresetData()
    }
    
    func generateNewPresetName() -> String? {
        var nextPresetName: String?
        let arrNames: NSMutableArray = NSMutableArray()
        for i in 1..<100 {
            arrNames.add(String(format: "Preset%d",i))
        }
        let arrPresetNamesDB = DBManager.shared.getAllPresetName()
        for i in 0..<arrNames.count {
                if (arrPresetNamesDB.contains(arrNames.object(at: i))) {
                    
                } else {
                    nextPresetName = arrNames.object(at: i) as? String
                    break
                }
            }
        return nextPresetName
    }
}
