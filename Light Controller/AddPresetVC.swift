//
//  AddPresetVC.swift
//  Light Controller
//
//  Created by Darshan Gajera on 5/28/19.
//  Copyright © 2019 Josh Jordan. All rights reserved.
//

import UIKit
import SQLite3

struct Preset: Codable {
    var id: Int?
    var preset_name: String?
    
    var bass_color: Int?
    var bass_bright: Int?
    var bass_color_options: Int?
    var bass_animation_option: Int?
    var bass_auto_animation: Int?
    
    var mid_color: Int?
    var mid_bright: Int?
    var mid_color_options: Int?
    var mid_animation_option: Int?
    var mid_auto_animation: Int?
    
    var trebel_color: Int?
    var trebel_bright: Int?
    var trebel_color_options: Int?
    var trebel_animation_option: Int?
    var trebel_auto_animation: Int?

}

class AddPresetVC: BaseVC, UITextFieldDelegate {
    
    var alertController: UIAlertController = UIAlertController()
    @IBOutlet weak var segment: UISegmentedControl!
    
    @IBOutlet weak var stackview: UIStackView!
    @IBOutlet weak var lblHeader: UILabel!
    @IBOutlet weak var colorSlider: UISlider!
    @IBOutlet weak var colorImage: UIImageView!
    @IBOutlet weak var colorView: UIView!
    @IBOutlet weak var brightnessSlider: UISlider!
    
    @IBOutlet weak var autoSwitch: UISwitch!
    @IBOutlet weak var colorPicker: UIPickerView!
    @IBOutlet weak var animationPicker: UIPickerView!
    
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    
    // MARK: Global Variables
    var presetName: String?
    var singlePreset: Preset?
    
    var strTitle: String?
    var colorOptions = ["Static Color", "Change with Auto Animation", "Change Every Beat", "Occasional Quick Change", "Occasional Fade Change", "Continuous Fade SLOW", "Continuous Fade FAST"]
    
    var animationOptions = ["Select Animation", "Blink", "Fade", "Strobe", "Scale Brightness", "Scale Linear", "Lasers", "Sparkle Explosion", "Section Switch", "Section Flash", "Scaled Scrolling", "Magic Stick", "Mixed - Scaled Scrolling", "Mixed - Lasers", "Constant - Solid", "Constant - Blink", "Constant - Pulse", "Constant - Sparkle", "Constant - Chasers", "Constant - Scroll", "OFF"]

    //MARK: View's Delegate
    override func viewDidLoad() {
        super.viewDidLoad()
        self.defaultSetting()
        self.setNavigation()
        self.btnClicks()
    }

    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if self.singlePreset?.id != nil {
            // edit
            self.btnSave.setTitle("UPDATE", for: .normal)
        }
        self.setvalueToControl(index: 0)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
    }
    
    //MARK: Other methods
    func setNavigation() {
        print("System label: \(UIColor.label)")
        self.setNavigationBar(title: strTitle as NSString?, titleImage: nil, leftImage: "\u{2039}".toUIImage(), rightImage: nil, leftTitle: nil, rightTitle: nil, isLeft: true, isRight: false, bgColor: UIColor.systemBackground, textColor: UIColor.label, leftClick: { (btnLeft) in
            self.navigationController?.popViewController(animated: true)
        }) { (btnRight) in
            
        }
    }
    
    func headerTitleChange(index: Int) {
        switch index {
        case 0:
            self.lblHeader.text = "Bass Light Settings"
            break
        case 1:
            self.lblHeader.text = "Mid Range Settings"
            break
        case 2:
            self.lblHeader.text = "Treble Light Settings"
            break
        default: break
            
        }
    }
    
    func defaultSetting() {
        self.headerTitleChange(index: 0)
        if self.singlePreset?.id != nil {
//            // edit
//            self.setvalueToControl(index: 0)
//            self.btnSave.setTitle("UPDATE", for: .normal)
        } else {
            // new
            self.btnSave.setTitle("SAVE", for: .normal)
            
            if AppPrefsManager.sharedInstance.isKeyExistInPreference(key: AppPrefsManager.sharedInstance.MAINPRESET) {
                let preset: Preset = AppPrefsManager.sharedInstance.getMainPreset()!
                singlePreset = Preset(id: nil, preset_name: nil, bass_color: preset.bass_color , bass_bright: preset.bass_bright, bass_color_options: preset.bass_color_options, bass_animation_option: preset.bass_animation_option, bass_auto_animation: preset.bass_auto_animation, mid_color: preset.mid_color, mid_bright: preset.mid_bright, mid_color_options: preset.mid_color_options, mid_animation_option: preset.mid_animation_option, mid_auto_animation: preset.mid_auto_animation, trebel_color: preset.trebel_color, trebel_bright: preset.trebel_bright, trebel_color_options: preset.trebel_color_options, trebel_animation_option: preset.trebel_animation_option, trebel_auto_animation: preset.trebel_auto_animation)
                }
        }
        
        brightnessSlider.isContinuous = true;
        brightnessSlider.addTarget(self, action: #selector(brightnessEndChanged(_:)), for: .valueChanged)
        
        colorSlider.isContinuous = true;
        colorSlider.addTarget(self, action: #selector(colorChanged(_:)), for: .valueChanged)
        
        self.colorPicker.dataSource = self
        self.colorPicker.delegate = self
        self.animationPicker.dataSource = self
        self.animationPicker.delegate = self
    }
    
    func btnClicks() {
        btnSave.addAction {
            if self.singlePreset?.id != nil {
                // edit
                self.btnSave.setTitle("UPDATE", for: .normal)
                self.updateRecord()
            } else {
                // new
                self.btnSave.setTitle("SAVE", for: .normal)
                self.addPresetName()
            }
        }
        btnCancel.addAction {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    //MARK: Segment Selections
    @IBAction func segmentValue(_ sender: UISegmentedControl) {
        self.setvalueToControl(index: sender.selectedSegmentIndex)
        self.headerTitleChange(index: sender.selectedSegmentIndex)
    }
    
    func setvalueToControl(index: Int) {
        print(singlePreset.dictionary)
        if index == 0 {
            // bass
            brightnessSlider.value = Float(singlePreset!.bass_bright!)
            colorSlider.value = Float(singlePreset!.bass_color!)
            autoSwitch.setOn(singlePreset!.bass_auto_animation != 0, animated: false)
            colorPicker.selectRow(singlePreset!.bass_color_options!, inComponent: 0, animated: false)
            animationPicker.selectRow(singlePreset!.bass_animation_option!, inComponent: 0, animated: false)
        } else if index == 1 {
            //mid
            brightnessSlider.value = Float(singlePreset!.mid_bright!)
            colorSlider.value = Float(singlePreset!.mid_color!)
            autoSwitch.setOn(singlePreset!.mid_auto_animation != 0, animated: false)
            colorPicker.selectRow(singlePreset!.mid_color_options!, inComponent: 0, animated: false)
            animationPicker.selectRow(singlePreset!.mid_animation_option!, inComponent: 0, animated: false)
        } else if index == 2 {
            //treble
            brightnessSlider.value = Float(singlePreset!.trebel_bright!)
            colorSlider.value = Float(singlePreset!.trebel_color!)
            autoSwitch.setOn(singlePreset!.trebel_auto_animation != 0, animated: false)
            colorPicker.selectRow(singlePreset!.trebel_color_options!, inComponent: 0, animated: false)
            animationPicker.selectRow(singlePreset!.trebel_animation_option!, inComponent: 0, animated: false)
        }
    }
    
    //MARK: Slider value change events
    @objc func colorChanged(_ sender: UISlider) {
        switch(segment.selectedSegmentIndex) {
        case 0:
            singlePreset?.bass_color = Int(sender.value)
            break;
        case 1:
            singlePreset?.mid_color = Int(sender.value)
            break;
        case 2:
            singlePreset?.trebel_color = Int(sender.value)
            break;
        default:
            break;
        }
    }
    
    @objc func brightnessEndChanged(_ sender: UISlider) {
        switch(segment.selectedSegmentIndex) {
        case 0:
            singlePreset?.bass_bright = Int(sender.value)
            break;
        case 1:
            singlePreset?.mid_bright = Int(sender.value)
            break;
        case 2:
            singlePreset?.trebel_bright = Int(sender.value)
            break;
        default:
            break;
        }
    }
    
    @IBAction func autoAnimation(_ sender: UISwitch) {
        switch(segment.selectedSegmentIndex) {
        case 0:
            singlePreset?.bass_auto_animation = sender.isOn == true ? 1 : 0
        break;
        case 1:
            singlePreset?.mid_auto_animation = sender.isOn == true ? 1 : 0
            break;
        case 2:
            singlePreset?.trebel_auto_animation = sender.isOn == true ? 1 : 0
            break;
        default:
            break;
        }
    }
    
    //MARK: Textfield's delegate
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let set = NSCharacterSet(charactersIn: "ABCDEFGHIJKLMONPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_: !@#$%&*()'").inverted
        return string.rangeOfCharacter(from: set) == nil
    }
  
    // Alertview for add name
    func addPresetName() {
        self.alertController = UIAlertController(title: "Add Preset Name", message: "", preferredStyle: .alert)
        self.alertController.addTextField { (txtPresetName) in
            txtPresetName.addTarget(self, action: #selector(self.renameAlertValueChange(txtF:)), for: .editingChanged)
            txtPresetName.delegate = self
            txtPresetName.autocapitalizationType = .words
            txtPresetName.keyboardType = .alphabet
            txtPresetName.text = self.presetName
            txtPresetName.placeholder = "Enter Preset Name"
        }
        let saveAction = UIAlertAction(title: "Save", style: .default, handler: { alert -> Void in
            let firstTextField = self.alertController.textFields![0] as UITextField
            if firstTextField.text!.count > 0 {
                    self.presetName = firstTextField.text!
                    self.singlePreset?.preset_name  = firstTextField.text!
                    self.insertRecord()
            } else {
                self.presentAlertWithTitle(presentStyle: .alert, title: GlobalConstants.APPNAME , message: ErrorMesssages.enterPresetName, options: "Ok") { (index) in
                    
                }
            }
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: {
            (action : UIAlertAction!) -> Void in })
        
        alertController.addAction(saveAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    @objc func renameAlertValueChange(txtF: UITextField){
        if txtF.text!.count <= 0 {
            alertController.actions[0].isEnabled = false
        } else {
            if self.presetName == txtF.text! {
                alertController.actions[0].isEnabled = true
            } else {
                if self.nameAlreadySaved(strName: txtF.text!)! {
                    alertController.actions[0].isEnabled = true
                } else {
                    alertController.actions[0].isEnabled = false
                }
            }
        }
    }
    
    
    
    // Check name is already saved or not (name is unique)
    func nameAlreadySaved(strName: String)-> Bool? {
        if DBManager.shared.nameAlreadyExist(strQuery: "SELECT * FROM \(TableName.presetMaster) WHERE \(TableField.preset_name) = ?", values: [strName]) {
            return false
        } else {
            if strName != "" {
             
                
                return true
            } else {
                return false
            }
        }
    }
    
    
    
    // save into database
    func insertRecord() {
        print("NEW:", singlePreset.dictionary)
       
        let queryString = String(format: "INSERT INTO %@ (%@, %@, %@, %@ ,%@, %@, %@, %@, %@, %@, %@, %@ ,%@, %@, %@, %@) VALUES ('%@',%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d)", TableName.presetMaster, TableField.preset_name,TableField.bass_color, TableField.bass_bright, TableField.bass_color_options , TableField.bass_animation_option,TableField.bass_auto_animation , TableField.mid_color , TableField.mid_bright, TableField.mid_color_options , TableField.mid_animation_option ,TableField.mid_auto_animation , TableField.trebel_color , TableField.trebel_bright , TableField.trebel_color_options , TableField.trebel_animation_option ,TableField.trebel_auto_animation,singlePreset!.preset_name!,singlePreset!.bass_color!,singlePreset!.bass_bright!,singlePreset!.bass_color_options!,singlePreset!.bass_animation_option!,singlePreset!.bass_auto_animation! == 0 ? 0 : 1,singlePreset!.mid_color!,singlePreset!.mid_bright!,singlePreset!.mid_color_options!,singlePreset!.mid_animation_option!,singlePreset!.mid_auto_animation! == 0 ? 0 : 1,singlePreset!.trebel_color!,singlePreset!.trebel_bright!,singlePreset!.trebel_color_options!,singlePreset!.trebel_animation_option!,singlePreset!.trebel_auto_animation! == 0 ? 0 : 1)
        
            if DBManager.shared.insertRecord(strInsertQuery: queryString) {
                self.presentAlertWithTitle(presentStyle: .alert, title: GlobalConstants.APPNAME, message: SuccessMesssages.save, options: "Ok") { (index) in
                    if index == 0 {
                        self.navigationController?.popViewController(animated: true)
                    }
                }
                print("preset saved successfully")
            }
    }
    
    // update preset
    func updateRecord() {
        if AppPrefsManager.sharedInstance.isKeyExistInPreference(key: AppPrefsManager.sharedInstance.SELECTEDPRESET) {
            let selectedPreset: Preset = AppPrefsManager.sharedInstance.getSelectedPreset()!
            if selectedPreset.preset_name == singlePreset?.preset_name {
                AppPrefsManager.sharedInstance.removeDataFromPreference(key: AppPrefsManager.sharedInstance.SELECTEDPRESET)
            }
        }
        
        print("UPDATE:", singlePreset.dictionary)
        let queryString = String(format: "UPDATE %@ SET %@ = ?, %@ = ?, %@ = ?, %@ = ? , %@ = ? ,%@ = ?, %@ = ?, %@ = ?, %@ = ?, %@ = ?, %@ = ?, %@ = ?, %@ = ?, %@ = ?, %@ = ? , %@ = ?  WHERE id = %d", TableName.presetMaster,TableField.preset_name,TableField.bass_color,TableField.bass_bright,TableField.bass_color_options,TableField.bass_animation_option,TableField.bass_auto_animation,TableField.mid_color,TableField.mid_bright,TableField.mid_color_options,TableField.mid_animation_option,TableField.mid_auto_animation,TableField.trebel_color,TableField.trebel_bright,TableField.trebel_color_options,TableField.trebel_animation_option,TableField.trebel_auto_animation,(singlePreset?.id!)!)
        
        let arrVal : [Any] = [singlePreset!.preset_name!, singlePreset!.bass_color!,singlePreset!.bass_bright!,singlePreset!.bass_color_options!,singlePreset!.bass_animation_option!,singlePreset!.bass_auto_animation! == 0 ? 0 : 1,singlePreset!.mid_color!,singlePreset!.mid_bright!,singlePreset!.mid_color_options!,singlePreset!.mid_animation_option!,singlePreset!.mid_auto_animation! == 0 ? 0 : 1,singlePreset!.trebel_color!,singlePreset!.trebel_bright!,singlePreset!.trebel_color_options!,singlePreset!.trebel_animation_option!,singlePreset!.trebel_auto_animation! == 0 ? 0: 1]
        
        if DBManager.shared.executeQuery(strQuery: queryString, values: arrVal) {
            self.presentAlertWithTitle(presentStyle: .alert, title: GlobalConstants.APPNAME, message: SuccessMesssages.update, options: "Ok") { (index) in
                if index == 0 {
                    self.navigationController?.popViewController(animated: true)
                }
            }
        } else {
            
        }
    }
 }

//MARK: PickerView delegate
extension AddPresetVC: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if(pickerView == colorPicker) {
            return colorOptions.count
        } else {
            return animationOptions.count
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if(pickerView == colorPicker) {
            return colorOptions[row]
        } else {
            return animationOptions[row]
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if(pickerView == colorPicker) {
            print("color picker \(row)")
            switch(segment.selectedSegmentIndex) {
            case 0:
                singlePreset?.bass_color_options = row
                break;
            case 1:
                singlePreset?.mid_color_options = row
                break;
            case 2:
                singlePreset?.trebel_color_options = row
                break;
            default:
                break;
            }
        } else {
            switch(segment.selectedSegmentIndex) {
            case 0:
                singlePreset?.bass_animation_option = row
                break;
            case 1:
                singlePreset?.mid_animation_option = row
                break;
            case 2:
                singlePreset?.trebel_animation_option = row
                break;
            default:
                break;
            }
        }
    }
}
