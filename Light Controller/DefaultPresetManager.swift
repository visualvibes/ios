import UIKit

struct ConfigPreset: Codable {
    var presetId: Int?
    var presetName: String?
    
    var bassColor: Int?
    var bassBright: Int?
    var bassColorOption: Int?
    var bassAnimationOption: Int?
    var bassAutoAnimation: Int?
    
    var midColor: Int?
    var midBright: Int?
    var midColorOption: Int?
    var midAnimationOption: Int?
    var midAutoAnimation: Int?
    
    var trebleColor: Int?
    var trebleBright: Int?
    var trebleColorOption: Int?
    var trebleAnimationOption: Int?
    var trebleAutoAnimation: Int?
}

class DefaultPresetManager {
    static func getDefaultPresetsFromConfig() -> [ConfigPreset] {
        var configPresets = [ConfigPreset]()
        do {
            let asset = NSDataAsset(name: "default_presets", bundle: Bundle.main)
            configPresets = try JSONDecoder().decode([ConfigPreset].self, from: asset!.data)
        } catch {
            print("Error deserializing default presets: \(error).")
        }

        return configPresets
    }
    static func syncDefaultPresets() {
        let configPresets = getDefaultPresetsFromConfig()
        let currentPresets = getAllPresets()
        for preset in configPresets {
            if !currentPresets.contains(where: { $0.preset_name == preset.presetName }) {
                insertPreset(preset: preset)
            }
        }
    }

    static func getAllPresets() -> [Preset] {
        var resultArray = NSMutableArray()
        DBManager.shared.getAllPresetData(withID: 0) { (result) in
            if result != nil {
                resultArray = NSMutableArray(array: result!)
            }
        }
        
        return resultArray.compactMap { $0 as? Preset }
    }

    static func insertPreset(preset: ConfigPreset) {
        let formatString = """
            INSERT INTO %@ ( \
                %@, %@, %@, %@ ,%@, %@, %@, %@, %@, %@, %@, %@ ,%@, %@, %@, %@ \
            ) VALUES ( \
                '%@', %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d \
            )
        """
        let queryString = String(format: formatString, 
            TableName.presetMaster, 
            TableField.preset_name,
            TableField.bass_color, 
            TableField.bass_bright, 
            TableField.bass_color_options, 
            TableField.bass_animation_option,
            TableField.bass_auto_animation, 
            TableField.mid_color, 
            TableField.mid_bright, 
            TableField.mid_color_options, 
            TableField.mid_animation_option,
            TableField.mid_auto_animation,
            TableField.trebel_color,
            TableField.trebel_bright,
            TableField.trebel_color_options,
            TableField.trebel_animation_option,
            TableField.trebel_auto_animation,
            preset.presetName!,
            preset.bassColor!,
            preset.bassBright!,
            preset.bassColorOption!,
            preset.bassAnimationOption!,
            preset.bassAutoAnimation! == 0 ? 0 : 1,
            preset.midColor!,
            preset.midBright!,
            preset.midColorOption!,
            preset.midAnimationOption!,
            preset.midAutoAnimation! == 0 ? 0 : 1,
            preset.trebleColor!,
            preset.trebleBright!,
            preset.trebleColorOption!,
            preset.trebleAnimationOption!,
            preset.trebleAutoAnimation! == 0 ? 0 : 1)
    
        if !DBManager.shared.insertRecord(strInsertQuery: queryString) {
            print("Error saving default preset \(preset.presetId as Int?)")
        }
    }
}
