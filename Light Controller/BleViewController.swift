//
//  FirstViewController.swift
//  Light Controller
//
//  Created by Josh Jordan on 5/25/17.
//  Copyright © 2017 Josh Jordan. All rights reserved.
//

import CoreBluetooth
import UIKit

class BleViewController: BaseVC {

    var tabbar:BleTabBarController!
    var address:String!
   
    
    @IBOutlet weak var btnPresets: UIButton!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var stateLabel: UILabel!
    @IBOutlet weak var dataValue: UILabel!
    @IBOutlet weak var fwUpdate: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tabbar = self.tabBarController as? BleTabBarController
        NotificationCenter.default.addObserver(self, selector: #selector(setChangedValue), name: .changeControllerValue, object: nil)
        address = String(String(tabbar.peripheral.identifier.uuidString).dropFirst(24))
        print("tabview didload \(tabbar.peripheral.name!)")
        self.nameLabel.text = "Name: \(tabbar.peripheral.name!)"
        self.addressLabel.text = "Address: \(address!)"
        self.stateLabel.text = "State: \(tabbar.stateString(val: tabbar.peripheral.state.rawValue))"
        tabbar.dataVal = dataValue
    }
    
    @IBAction func btnPresetClick(_ sender: Any) {
        let preset = Storyboard.main.storyboard().instantiateViewController(withIdentifier: Identifier.Preset.rawValue) as! PresetVC
        self.navigationController?.pushViewController(preset, animated: true)
    }
    
    
    @objc func setChangedValue(notification: NSNotification) {
        let dicPresetData = notification.userInfo! as NSDictionary
        let signlePreset: Preset = dicPresetData.value(forKey: "selectedPreset") as! Preset
        
        tabbar.trebleBrightness = (signlePreset.trebel_bright)!
        tabbar.trebleColor = (signlePreset.trebel_color)!
        tabbar.trebleAuto = (signlePreset.trebel_auto_animation)!
        tabbar.trebleOption = (signlePreset.trebel_color_options)!
        tabbar.trebleAnim = (signlePreset.trebel_animation_option)!
        
        tabbar.midBrightness = (signlePreset.mid_bright)!
        tabbar.midColor = (signlePreset.mid_color)!
        tabbar.midAuto = (signlePreset.mid_auto_animation)!
        tabbar.midOption = (signlePreset.mid_color_options)!
        tabbar.midAnim = (signlePreset.mid_animation_option)!
        
        tabbar.bassBrightness = (signlePreset.bass_bright)!
        tabbar.bassColor = (signlePreset.bass_color)!
        tabbar.bassAuto = (signlePreset.bass_auto_animation)!
        tabbar.bassOption = (signlePreset.bass_color_options)!
        tabbar.bassAnim = (signlePreset.bass_animation_option)!
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        self.navigationController?.tabBarController?.tabBar.isHidden = false

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }

    @IBAction func doFwUpdate(_ sender: Any) {
        print("do firmware update")
        if let path = Bundle.main.path(forResource: "blinkfast", ofType: "hex") {
            do {
                let data = try String(contentsOfFile: path, encoding: .utf8)
                let myStrings = data.components(separatedBy: .newlines)
                for _ in myStrings {
                    //print("\(line.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines))")
                }
            } catch {
                print(error)
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func disconnectButtonPress(_ sender: Any) {
        print("disconnect pressed")
        if(tabbar.manager == nil || tabbar.peripheral == nil) {
            print("manager nill? \(tabbar.manager == nil), peripheral nil? \(tabbar.peripheral == nil)")
        } else {
            tabbar.bleDisconnect()
        }
    }
    

    
 


}

