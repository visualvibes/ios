//
//  NBSBaseViewController.swift
//  DemoBank
//
//  Created by Vikram on 11/26/16.
//  Copyright © 2016 SDL. All rights reserved.
//

import Foundation
import UIKit
import SQLite3

//swiftlint:disable all
typealias LeftButton = (_ left: UIButton) -> Void
typealias RightButton = (_ right: UIButton) -> Void

class ClosureSleeve {
    let closure: () -> ()
    
    init(attachTo: AnyObject, closure: @escaping () -> ()) {
        self.closure = closure
        objc_setAssociatedObject(attachTo, "[\(arc4random())]", self, .OBJC_ASSOCIATION_RETAIN)
    }
    @objc func invoke() {
        closure()
    }
}

class BaseVC: UIViewController {

    var db: OpaquePointer?
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func showAlert(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: { (_) in
        }))
        self.present(alert, animated: true, completion: nil)
    }

    // MARK: Navigation
    func setNavigationBar(title: NSString? ,
                        titleImage: UIImage?,
                        leftImage: UIImage? ,
                        rightImage: UIImage?,
                        leftTitle: String?,
                        rightTitle: String?,
                        isLeft: Bool ,
                        isRight: Bool,
                        bgColor: UIColor ,
                        textColor: UIColor,
                        leftClick: @escaping LeftButton ,
                        rightClick: @escaping RightButton)  {
        
        self.navigationItem.hidesBackButton = true

        // Left Item
        let btnLeft: UIButton = UIButton(type: .custom)
        btnLeft.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
        btnLeft.imageView?.contentMode = .scaleAspectFit
        let addImg = leftImage
        if leftTitle != nil {
            btnLeft.setTitle(leftTitle, for: .normal)
            self.addConstaintsWithWidth(width: 50, height: 30, btn: btnLeft)
        } else {
            btnLeft.setImage(addImg, for: .normal)
            self.addConstaintsWithWidth(width: 25, height: 25, btn: btnLeft)
        }
        btnLeft.sendActions(for: .touchUpInside)
        let leftBarItem: UIBarButtonItem = UIBarButtonItem(customView: btnLeft)
        if isLeft {
            self.navigationItem.leftBarButtonItem = leftBarItem
        }
        
        btnLeft.addAction {
            leftClick(btnLeft)
        }
        // Right Item
        let btnRight: UIButton = UIButton(type: .custom)
        btnRight.frame = CGRect(x: self.view.frame.size.width, y: 0, width: 25, height: 25)
        btnRight.imageView?.contentMode = .scaleAspectFit
        let addImg1 = rightImage
        if rightTitle != nil {
            self.addConstaintsWithWidth(width: 70, height: 30, btn: btnRight)
            btnRight.titleLabel?.font = UIFont.systemFont(ofSize: 18.0)
            btnRight.titleLabel?.sizeToFit()
            btnRight.setTitleColor(UIColor.label, for: .normal)
            btnRight.setTitle(rightTitle, for: .normal)
        } else {
            self.addConstaintsWithWidth(width: 25, height: 25, btn: btnRight)
            btnRight.setImage(addImg1, for: .normal)
        }
        btnRight.sendActions(for: .touchUpInside)
        let rightBarItem: UIBarButtonItem = UIBarButtonItem(customView: btnRight)
        if isRight {
            self.navigationItem.rightBarButtonItem = rightBarItem
        }
        
            btnRight.addAction {
                rightClick(btnRight)
            }
        
        // title
        if title == nil {
            let xRect: CGFloat = self.view.frame.size.width/2 - 50
            let yRect: CGFloat = self.view.frame.size.height/2 - 40
            let rectImgView: CGRect = CGRect(x: xRect, y: yRect, width: 100, height: 40.0)
            let imgViewTitle = UIImageView(image: titleImage)
            imgViewTitle.backgroundColor = UIColor.clear
            imgViewTitle.contentMode = .scaleAspectFit
            imgViewTitle.frame = rectImgView
            self.navigationItem.titleView = imgViewTitle
        } else {
            let xRect: CGFloat = 0
            let yRect: CGFloat = 0
            let widthRect: CGFloat = self.view.frame.size.width
            let heightRect: CGFloat = 40
            let rectLabel: CGRect = CGRect(x: xRect, y: yRect, width: widthRect, height: heightRect)
            let  lblNavigationTitleLabel = UILabel(frame: rectLabel) as UILabel
            lblNavigationTitleLabel.text = title! as String
            lblNavigationTitleLabel.font = UIFont.systemFont(ofSize: 18.0)
            lblNavigationTitleLabel.setScalable = true
            lblNavigationTitleLabel.textColor = textColor
            lblNavigationTitleLabel.textAlignment = .center
            lblNavigationTitleLabel.frame = CGRect(x: 100, y: 0, width: 100, height: 100)
            self.navigationItem.titleView = lblNavigationTitleLabel
        }
        self.navigationController?.navigationBar.barTintColor = bgColor
        self.navigationController?.navigationBar.isTranslucent = false
        
//        self.addCurvedNavigationBar(backgroundColor: bgColor, curveRadius: 15.0, shadowColor: UIColor.clear, shadowRadius: 2, heightOffset: 0) //swiftLint: disable control_statement
    }

    @objc func btnLeftMenuOpen(sender: UIButton) {
        
    }

    @objc func btnRightMenuOpen(sender: UIButton) {

    }

    func addConstaintsWithWidth(width: CGFloat ,height: CGFloat, btn: UIButton) {
        NSLayoutConstraint(item: btn, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: width).isActive = true
        NSLayoutConstraint(item: btn, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: height).isActive = true
    }
    
    func delayWithSeconds(_ seconds: Double, completion: @escaping () -> ()) {
        DispatchQueue.main.asyncAfter(deadline: .now() + seconds) {
            completion()
        }
    }
}

extension UIControl {
    func addAction(for controlEvents: UIControl.Event = .primaryActionTriggered, action: @escaping () -> ()) {
        let sleeve = ClosureSleeve(attachTo: self, closure: action)
        addTarget(sleeve, action: #selector(ClosureSleeve.invoke), for: controlEvents)
    }
}
