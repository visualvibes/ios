//
//  SecondViewController.swift
//  Light Controller
//
//  Created by Josh Jordan on 5/25/17.
//  Copyright © 2017 Josh Jordan. All rights reserved.
//

import UIKit

extension Notification.Name {
    static let changeControllerValue = Notification.Name("changeControllerValue")
}
class RangeViewController: BaseVC, UIPickerViewDataSource, UIPickerViewDelegate {
    var tabbar:BleTabBarController!
    
    @IBOutlet weak var stackview: UIStackView!
    @IBOutlet weak var colorSlider: UISlider!
    @IBOutlet weak var colorImage: UIImageView!
    @IBOutlet weak var colorView: UIView!
    @IBOutlet weak var brightnessSlider: UISlider!
    
    @IBOutlet weak var autoSwitch: UISwitch!
    @IBOutlet weak var colorPicker: UIPickerView!
    @IBOutlet weak var animationPicker: UIPickerView!
    
    var mainPreset: Preset?
    var colorOptions = ["Static Color", "Change with Auto Animation", "Change Every Beat", "Occasional Quick Change", "Occasional Fade Change", "Continuous Fade SLOW", "Continuous Fade FAST"]
    
    var animationOptions = ["Select Animation", "Blink", "Fade", "Strobe", "Scale Brightness", "Scale Linear", "Lasers", "Sparkle Explosion", "Section Switch", "Section Flash", "Scaled Scrolling", "Magic Stick", "Mixed - Scaled Scrolling", "Mixed - Lasers", "Constant - Solid", "Constant - Blink", "Constant - Pulse", "Constant - Sparkle", "Constant - Chasers", "Constant - Scroll", "OFF"]

    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        self.navigationController?.tabBarController?.tabBar.isHidden = false
        
        brightnessSlider.isContinuous = true;
        brightnessSlider.addTarget(self, action: #selector(brightnessEndChanged(_:)), for: .touchUpInside)
        colorSlider.isContinuous = true;
        colorSlider.addTarget(self, action: #selector(colorChanged(_:)), for: .touchUpInside)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.colorPicker.dataSource = self
        self.colorPicker.delegate = self
        self.animationPicker.dataSource = self
        self.animationPicker.delegate = self
        tabbar = self.tabBarController as? BleTabBarController
        if AppPrefsManager.sharedInstance.isKeyExistInPreference(key: AppPrefsManager.sharedInstance.MAINPRESET) {
            mainPreset = AppPrefsManager.sharedInstance.getMainPreset()!
        }
    }
   
    @objc func setChangedValue(notification: NSNotification) {
            let dicPresetData = notification.object as! NSDictionary
            let signlePreset: Preset = dicPresetData.value(forKey: "selectedPreset") as! Preset
        
            tabbar.trebleBrightness = (signlePreset.trebel_bright)!
            tabbar.trebleColor = (signlePreset.trebel_color)!
            tabbar.trebleAuto = (signlePreset.trebel_auto_animation)!
            tabbar.trebleOption = (signlePreset.trebel_color_options)!
            tabbar.trebleAnim = (signlePreset.trebel_animation_option)!
            
            tabbar.midBrightness = (signlePreset.mid_bright)!
            tabbar.midColor = (signlePreset.mid_color)!
            tabbar.midAuto = (signlePreset.mid_auto_animation)!
            tabbar.midOption = (signlePreset.mid_color_options)!
            tabbar.midAnim = (signlePreset.mid_animation_option)!
            
            tabbar.bassBrightness = (signlePreset.bass_bright)!
            tabbar.bassColor = (signlePreset.bass_color)!
            tabbar.bassAuto = (signlePreset.bass_auto_animation)!
            tabbar.bassOption = (signlePreset.bass_color_options)!
            tabbar.bassAnim = (signlePreset.bass_animation_option)!
        }
 
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func viewDidAppear(_ animated: Bool) {
        print("selected index: \(tabbar.selectedIndex)")
        if AppPrefsManager.sharedInstance.isKeyExistInPreference(key: AppPrefsManager.sharedInstance.MAINPRESET) {
            mainPreset = AppPrefsManager.sharedInstance.getMainPreset()!
        }
        switch(tabbar.selectedIndex) {
        case 1: //bass
            brightnessSlider.value = Float(tabbar.bassBrightness)
            colorSlider.value = Float(tabbar.bassColor)
            autoSwitch.setOn(tabbar.bassAuto != 0, animated: false)
            colorPicker.selectRow(tabbar.bassOption, inComponent: 0, animated: false)
            animationPicker.selectRow(tabbar.bassAnim, inComponent: 0, animated: false)
            break;
        case 2: //mid
            brightnessSlider.value = Float(tabbar.midBrightness)
            colorSlider.value = Float(tabbar.midColor)
            autoSwitch.setOn(tabbar.midAuto != 0, animated: false)
            colorPicker.selectRow(tabbar.midOption, inComponent: 0, animated: false)
            animationPicker.selectRow(tabbar.midAnim, inComponent: 0, animated: false)
            break;
        case 3: //trebel
            brightnessSlider.value = Float(tabbar.trebleBrightness)
            colorSlider.value = Float(tabbar.trebleColor)
            autoSwitch.setOn(tabbar.trebleAuto != 0, animated: false)
            colorPicker.selectRow(tabbar.trebleOption, inComponent: 0, animated: false)
            animationPicker.selectRow(tabbar.trebleAnim, inComponent: 0, animated: false)

            break;
        default:
            break;
        }
    }
    
    @objc func colorChanged(_ sender: UISlider) {
        print("color changed \(tabbar.selectedIndex)")
        switch(tabbar.selectedIndex) {
        case 1:
            tabbar.bleFifo.append((3, Data([11, 0x00, UInt8(sender.value)])))
            tabbar.bassColor = Int(sender.value)
            mainPreset?.bass_color = tabbar.bassColor
            self.animationValueSet()
            tabbar.nextBleMessage()
            break;
        case 2:
            tabbar.bleFifo.append((3, Data([21, 0x00, UInt8(sender.value)])))
            tabbar.midColor = Int(sender.value)
            mainPreset?.mid_color = tabbar.midColor
            self.animationValueSet()
            tabbar.nextBleMessage()
            break;
        case 3:
            tabbar.bleFifo.append((3, Data([31, 0x00, UInt8(sender.value)])))
            tabbar.trebleColor = Int(sender.value)
            mainPreset?.trebel_color = tabbar.trebleColor
            self.animationValueSet()
            tabbar.nextBleMessage()
             break;
        default:
            break;
        }
    }
    
    @objc func brightnessEndChanged(_ sender: UISlider) {
        print("brightness changed for \(tabbar.selectedIndex) to \(Int(sender.value))")
        switch(tabbar.selectedIndex) {
        case 1:
            tabbar.bleFifo.append((3, Data([12, 0x00, UInt8(sender.value)])))
            tabbar.bassBrightness = Int(sender.value)
            mainPreset?.bass_bright = tabbar.bassBrightness
            self.animationValueSet()
            tabbar.nextBleMessage()
            break;
        case 2:
            tabbar.bleFifo.append((3, Data([22, 0x00, UInt8(sender.value)])))
            tabbar.midBrightness = Int(sender.value)
            mainPreset?.mid_bright = tabbar.midBrightness
            self.animationValueSet()
            tabbar.nextBleMessage()
            break;
        case 3:
            tabbar.bleFifo.append((3, Data([32, 0x00, UInt8(sender.value)])))
            tabbar.trebleBrightness = Int(sender.value)
            mainPreset?.trebel_bright = tabbar.trebleBrightness
            self.animationValueSet()
            tabbar.nextBleMessage()
          break;
        default:
            break;
        }
    }
    
    
    @IBAction func autoAnimation(_ sender: UISwitch) {
        print("auto animation for \(tabbar.selectedIndex) to \(sender.isOn)")
        switch(tabbar.selectedIndex) {
        case 1:
            tabbar.bassAuto = sender.isOn ? 1 : 0
            tabbar.bleFifo.append((3, Data([18, 0x00, UInt8(tabbar.bassAuto)])))
            mainPreset?.bass_auto_animation = tabbar.bassAuto
            self.animationValueSet()
            tabbar.nextBleMessage()
            break;
        case 2:
            tabbar.midAuto = sender.isOn ? 1 : 0
            tabbar.bleFifo.append((3, Data([28, 0x00, UInt8(tabbar.midAuto)])))
            mainPreset?.mid_auto_animation = tabbar.midAuto
            self.animationValueSet()
            tabbar.nextBleMessage()
            break;
        case 3:
            tabbar.trebleAuto = sender.isOn ? 1 : 0
            tabbar.bleFifo.append((3, Data([38, 0x00, UInt8(tabbar.trebleAuto)])))
            mainPreset?.trebel_auto_animation = tabbar.trebleAuto
            self.animationValueSet()
            tabbar.nextBleMessage()
            break;
        default:
            break;
        }
    }

    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if(pickerView == colorPicker) {
            return colorOptions.count
        } else {
            return animationOptions.count
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if(pickerView == colorPicker) {
            return colorOptions[row]
        } else {
            return animationOptions[row]
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if(pickerView == colorPicker) {
            print("color picker \(row)")
            switch(tabbar.selectedIndex) {
            case 1:
                tabbar.bassOption = row
                tabbar.bleFifo.append((3, Data([17, 0x00, UInt8(row)])))
                mainPreset?.bass_color_options = tabbar.bassOption
                self.animationValueSet()
                tabbar.nextBleMessage()
               break;
            case 2:
                tabbar.midOption = row
                tabbar.bleFifo.append((3, Data([27, 0x00, UInt8(row)])))
                mainPreset?.mid_color_options = tabbar.midOption
                self.animationValueSet()
                tabbar.nextBleMessage()
                break;
            case 3:
                tabbar.trebleOption = row
                tabbar.bleFifo.append((3, Data([37, 0x00, UInt8(row)])))
                mainPreset?.trebel_color_options = tabbar.trebleOption
                self.animationValueSet()
                tabbar.nextBleMessage()
                break;
            default:
                break;
            }
            
        } else {
            print("animation picker \(row)")
            switch(tabbar.selectedIndex) {
            case 1:
                tabbar.bassAnim = row
                tabbar.bleFifo.append((3, Data([10, 0x00, UInt8(row + 9)])))
                mainPreset?.bass_animation_option = row
                self.animationValueSet()
                self.tabbar.nextBleMessage()
                break;
            case 2:
                tabbar.midAnim = row
                tabbar.bleFifo.append((3, Data([20, 0x00, UInt8(row + 9)])))
                mainPreset?.mid_animation_option = row
                self.animationValueSet()
                self.tabbar.nextBleMessage()
                break;
            case 3:
                tabbar.trebleAnim = row
                tabbar.bleFifo.append((3, Data([30, 0x00, UInt8(row + 9)])))
                mainPreset?.trebel_animation_option = row
                self.animationValueSet()
                self.tabbar.nextBleMessage()
                break;
            default:
                break;
            }
        }
    }
    
    func animationValueSet() {
        if tabbar.bassAnim == 20 {
            mainPreset?.bass_animation_option = tabbar.bassAnim
        }
        
        if tabbar.trebleAnim == 20 {
            mainPreset?.trebel_animation_option = tabbar.trebleAnim
        }
        
        if tabbar.midAnim == 20 {
            mainPreset?.mid_animation_option = tabbar.midAnim
        }
        
        if tabbar.bassAuto == 0 {
            mainPreset?.bass_auto_animation = tabbar.bassAuto
        }
        
        if tabbar.trebleAuto == 0 {
            mainPreset?.trebel_auto_animation = tabbar.trebleAuto
        }
        
        if tabbar.midAuto == 0 {
            mainPreset?.mid_auto_animation = tabbar.midAuto
        }
        AppPrefsManager.sharedInstance.setValueChange(obj: true)
        AppPrefsManager.sharedInstance.setMainPreset(obj: mainPreset?.dictionary as AnyObject)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    @IBAction func btnPresetClick(_ sender: Any) {
        let preset = Storyboard.main.storyboard().instantiateViewController(withIdentifier: Identifier.Preset.rawValue) as! PresetVC
        self.navigationController?.pushViewController(preset, animated: true)
    }
    
}

