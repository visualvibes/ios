//
//  BleTabBarController.swift
//  Light Controller
//
//  Created by Josh Jordan on 5/28/17.
//  Copyright © 2017 Josh Jordan. All rights reserved.
//

import CoreBluetooth
import UIKit

class BleTabBarController: UITabBarController, CBCentralManagerDelegate, CBPeripheralDelegate {
    
    let UART_SERVICE_UUID = CBUUID(string: "6e400001-b5a3-f393-e0a9-e50e24dcca9e")
    let ENABLE_CHAR_UUID = CBUUID(string: "6e400008-b5a3-f393-e0a9-e50e24dcca9e")
    let FLOW_CHAR_UUID = CBUUID(string: "6e400006-b5a3-f393-e0a9-e50e24dcca9e")
    let BAUD_CHAR_UUID = CBUUID(string: "6e400004-b5a3-f393-e0a9-e50e24dcca9e")
    let TX_CHAR_UUID = CBUUID(string: "6e400002-b5a3-f393-e0a9-e50e24dcca9e")
    let RX_CHAR_UUID = CBUUID(string: "6e400003-b5a3-f393-e0a9-e50e24dcca9e")
    
    let CFG_SERVICE_UUID = CBUUID(string: "2413b33f-707f-90bd-2045-2ab8807571b7")
    let CTRL_CHAR_UUID = CBUUID(string: "2413b43f-707f-90bd-2045-2ab8807571b7")

    
    var peripheral:CBPeripheral!
    var manager:CBCentralManager!
    
    var enChar:CBCharacteristic!
    var flowChar:CBCharacteristic!
    var baudChar:CBCharacteristic!
    var ctrlChar:CBCharacteristic!
    var txChar:CBCharacteristic!
    var rxChar:CBCharacteristic!
    
    var charsFound:Int = 0
    
    var initState:Int = 0 //when downloading settings, 1 indicates first part is received, 2 indicates all settings received
    var initMessage:[Int] = []
    var filter:Int = 0
    var bassAnim:Int = 9 //unselected value is 9, animation options start at 10
    var midAnim:Int = 9
    var trebleAnim:Int = 9
    var bassColor:Int = 0
    var midColor:Int = 0
    var trebleColor:Int = 0
    var bassBrightness:Int = 0
    var midBrightness:Int = 0
    var trebleBrightness:Int = 0
    var bassOption:Int = 0
    var bassAuto:Int = 0
    var midOption:Int = 0
    var midAuto:Int = 0
    var trebleOption:Int = 0
    var trebleAuto:Int = 0
    var bassCount:Int = 0
    var midCount:Int = 0
    var trebleCount:Int = 0
    
    var dataVal:UILabel!
    
    //fifo contains index to characteristic and byte array of message
    var bleFifo = Array<(UInt8, Data)>()
    
    func nextBleMessage() {
        if bleFifo.count > 0 {
            switch(bleFifo[0].0) {
            case 0:
                peripheral.writeValue(bleFifo[0].1, for: enChar, type: CBCharacteristicWriteType.withResponse)
                break;
            case 1:
                peripheral.writeValue(bleFifo[0].1, for: flowChar, type: CBCharacteristicWriteType.withResponse)
                break;
            case 2:
                peripheral.writeValue(bleFifo[0].1, for: baudChar, type: CBCharacteristicWriteType.withResponse)
                break;
            case 3:
                peripheral.writeValue(bleFifo[0].1, for: txChar, type: CBCharacteristicWriteType.withResponse)
                break;
            case 4:
                peripheral.writeValue(bleFifo[0].1, for: rxChar, type: CBCharacteristicWriteType.withResponse)
                break;
            case 5:
                peripheral.writeValue(bleFifo[0].1, for: ctrlChar, type: CBCharacteristicWriteType.withResponse)
                break;
            default:
                break;
            }
            bleFifo.remove(at: 0)
        }
        else {
            print("ble fifo empty")
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        charsFound = 0
        peripheral.delegate = self
        manager.delegate = self
        if peripheral.services != nil {
            for service in peripheral.services! {
                let thisService = service as CBService
                
                if service.uuid == UART_SERVICE_UUID {
                    print("found uart service")
                    peripheral.discoverCharacteristics(
                        nil,
                        for: thisService
                    )
                }
                else if service.uuid == CFG_SERVICE_UUID {
                    print("found cfg service")
                    peripheral.discoverCharacteristics(nil, for: thisService)
                }
            }
            print("tab bar viewdidload \(peripheral.name!)")
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func bleDisconnect() {
        manager.cancelPeripheralConnection(peripheral)
        print("ble status \(stateString(val: peripheral.state.rawValue))")
    }
    

    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        if central.state == CBManagerState.poweredOn {
            print("Scan start tabbar")
            central.scanForPeripherals(withServices: nil, options: nil)
        } else {
            print("Bluetooth not available. tabbar")
        }
    }
    
    
    func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {
        print("shouldn't scan in tabbar")
    }
    
    func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
        print("didConnectPeripheral tabbar")
        peripheral.discoverServices(nil)
    }
    
    func centralManager(_ central: CBCentralManager,
                                 didDisconnectPeripheral peripheral: CBPeripheral,
                                 error: Error?){
        print("ble disconnected event")
        self.performSegue(withIdentifier: "disconnectedSegue", sender: self)

    }
    
    func centralManager(_ central: CBCentralManager, didFailToConnect peripheral: CBPeripheral) {
        print("connect failed tabbar")
    }
    
    func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?) {
        print("shouldn't discover services in tabbar")

    }
    
    
    func peripheral(_ peripheral: CBPeripheral,
                    didDiscoverCharacteristicsFor service: CBService,
                    error: Error?){
        print("discovered characteristics")
        for characteristic in service.characteristics! {
            if characteristic.uuid == ENABLE_CHAR_UUID {
                enChar = characteristic
                print("found enChar")
                charsFound += 1
            }
            else if characteristic.uuid == FLOW_CHAR_UUID {
                flowChar = characteristic
                print("found flowChar")
                charsFound += 1
                
            }
            else if characteristic.uuid == BAUD_CHAR_UUID {
                baudChar = characteristic
                print("found baudChar")
                charsFound += 1
            }
            else if characteristic.uuid == TX_CHAR_UUID {
                txChar = characteristic
                print("found txChar")
                charsFound += 1
            }
            else if characteristic.uuid == RX_CHAR_UUID {
                rxChar = characteristic
                print("found rxChar")
                charsFound += 1
                peripheral.setNotifyValue(
                    true,
                    for: characteristic
                )
            }
            else if characteristic.uuid == CTRL_CHAR_UUID {
                ctrlChar = characteristic
                print("found ctrlChar")
                charsFound += 1
            }
            if(charsFound == 6) {
                initState = 0
                charsFound = 0
                bleFifo.append((0, Data([0x01]))) //enable on
                bleFifo.append((1, Data([0x00]))) //flow off
                bleFifo.append((2, Data([0x00, 0x01, 0xC2, 0x00]))) //baud rate
                bleFifo.append((3, Data([0xff, 0xff, 0xff])))
                nextBleMessage()
            }
        }
    }
    
    let CHexLookup : [Character] =
        [ "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F" ]
    public func byteArrayToHexString(_ byteArray : [UInt8]) -> String {
        
        var stringToReturn = ""
        
        for oneByte in byteArray {
            let asInt = Int(oneByte)
            stringToReturn.append(CHexLookup[asInt >> 4])
            stringToReturn.append(CHexLookup[asInt & 0x0f])
        }
        return stringToReturn
    }
    
    func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor characteristic: CBCharacteristic, error: Error?)
    {
        if (characteristic.uuid == RX_CHAR_UUID)
        {
            let updateData = [UInt8](characteristic.value!)
            if(dataVal != nil) {
                print("Characteristic - \(characteristic)")
                print("rxChar: - \(byteArrayToHexString(updateData))")

                if initState == 0  || initState == 1 {
                    for value in updateData {
                        initMessage.append(Int(value))
                    }
                    initState += 1;
                } else {
                    dataVal.text = byteArrayToHexString(updateData)
                }

                if (initMessage.endIndex == 22) {
                    filter = initMessage[0]
                    print("Filter: \(filter)")
                    bassAnim = max(initMessage[1] - 9, 0)
                    print("Bass animation: \(bassAnim)")
                    midAnim = max(initMessage[2] - 9, 0)
                    print("Mid animation: \(midAnim)")
                    trebleAnim = max(initMessage[3] - 9, 0)
                    print("Treble animation: \(trebleAnim)")
                    bassColor = initMessage[4]
                    print("Bass color: \(bassColor)")
                    midColor = initMessage[5]
                    print("Mid color: \(midColor)")
                    trebleColor = initMessage[6]
                    print("Treble color: \(trebleColor)")
                    bassBrightness = initMessage[7]
                    print("Bass brightness: \(bassBrightness)")
                    midBrightness = initMessage[8]
                    print("Mid brightness: \(midBrightness)")
                    trebleBrightness = initMessage[9]
                    print("Treble brightness: \(trebleBrightness)")
                    bassOption = initMessage[10]
                    print("Bass color option: \(bassOption)")
                    bassAuto = initMessage[11]
                    print("Bass auto animation: \(bassAuto)")
                    midOption = initMessage[12]
                    print("Mid color option: \(midOption)")
                    midAuto = initMessage[13]
                    print("Mid auto animation: \(midAuto)")
                    trebleOption = initMessage[14]
                    print("Treble color option: \(trebleOption)")
                    trebleAuto = initMessage[15]
                    print("Treble auto animation: \(trebleAuto)")
                    bassCount = initMessage[16] << 8 + initMessage[17]
                    print("Bass LED count: \(bassCount)")
                    midCount = initMessage[18] << 8 + initMessage[19]
                    print("Mid LED count: \(midCount)")
                    trebleCount = initMessage[20] << 8 + initMessage[21]
                    print("Treble LED count: \(trebleCount)")
                }

                let pre = Preset(id: 0, 
                    preset_name: "main", 
                    bass_color: bassColor, 
                    bass_bright: bassBrightness, 
                    bass_color_options: bassOption, 
                    bass_animation_option: bassAnim, 
                    bass_auto_animation: bassAuto, 
                    mid_color: midColor, 
                    mid_bright: midBrightness, 
                    mid_color_options: midOption, 
                    mid_animation_option: midAnim, 
                    mid_auto_animation: midAuto, 
                    trebel_color: trebleColor, 
                    trebel_bright: trebleBrightness, 
                    trebel_color_options: trebleOption, 
                    trebel_animation_option: trebleAnim, 
                    trebel_auto_animation: trebleAuto
                )
                AppPrefsManager.sharedInstance.setMainPreset(obj: pre.dictionary as AnyObject)
            }
        }
    }
    
    func peripheral(_ peripheral: CBPeripheral,
                    didWriteValueFor characteristic: CBCharacteristic,
                    error: Error?){
//        print("tx complete")
        nextBleMessage()
    }
    


    func stateString(val:Int) -> String {
        switch(val) {
        case 0:
            return "disconnected"
        case 1:
            return "connecting"
        case 2:
            return "connected"
        case 3:
            return "disconnecting"
        default:
            return "unknown state"
        }
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        switch segue.identifier! {
        default:
            break
        }
        
        print("tab bar segue \(segue.identifier!)")
    }
    

}
