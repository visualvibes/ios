//
//  DBManager.swift
//  Light Controller
//
//  Created by Darshan Gajera on 6/5/19.
//  Copyright © 2019 Josh Jordan. All rights reserved.
//

import UIKit
import FMDB


class DBManager: NSObject {
    static let shared = DBManager()
    let databaseFileName = "vivi.sqlite"
    var pathToDatabase: String!
    var database: FMDatabase!
   
    override init() {
        super.init()
        
        let documentsDirectory = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString) as String
        pathToDatabase = documentsDirectory.appending("/\(databaseFileName)")
    }
    
    func createDatabase() -> Bool {
        var created = false
        
        if !FileManager.default.fileExists(atPath: pathToDatabase) {
            database = FMDatabase(path: pathToDatabase!)
            
            if database != nil {
                // Open the database.
                if database.open() {
                    let createMoviesTableQuery = String(format: "CREATE TABLE IF NOT EXISTS %@ (%@ INTEGER PRIMARY KEY, %@ TEXT NOT NULL, %@ INTEGER, %@ INTEGER, %@ INTEGER, %@ INTEGER,%@ INTEGER, %@ INTEGER, %@ INTEGER, %@ INTEGER, %@ INTEGER, %@ INTEGER, %@ INTEGER, %@ INTEGER, %@ INTEGER, %@ INTEGER,%@ INTEGER)", TableName.presetMaster,TableField.id,TableField.preset_name,TableField.bass_color,TableField.bass_bright,TableField.bass_color_options,TableField.bass_animation_option,TableField.bass_auto_animation,TableField.mid_color,TableField.mid_bright,TableField.mid_color_options,TableField.mid_animation_option,TableField.mid_auto_animation,TableField.trebel_color,TableField.trebel_bright,TableField.trebel_color_options,TableField.trebel_animation_option,TableField.trebel_auto_animation)
                    
                    do {
                        try database.executeUpdate(createMoviesTableQuery, values: nil)
                        created = true
                    }
                    catch {
                        print("Could not create table.")
                        print(error.localizedDescription)
                    }
                    
                    // At the end close the database.
                    database.close()
                }
                else {
                    print("Could not open the database.")
                }
            }
        }
        
        return created
    }

    func openDatabase() -> Bool {
        if database == nil {
            if FileManager.default.fileExists(atPath: pathToDatabase) {
                database = FMDatabase(path: pathToDatabase)
            }
        }
        
        if database != nil {
            if database.open() {
                return true
            }
        }
        
        return false
    }

    func insertRecord(strInsertQuery: String) -> Bool {
        if openDatabase() {
                if !database.executeStatements(strInsertQuery) {
                    print("Failed to insert initial data into the database.")
                    print(database.lastError(), database.lastErrorMessage())
                    return false
                }
            }
            database.close()
        return true
    }
    
    func getAllPresetData(withID ID: Int, completionHandler: (_ preser: NSMutableArray?) -> Void) {
        let arrData: NSMutableArray = NSMutableArray()
        var singlePreset: Preset = Preset()
        if openDatabase() {
            var query: String? = "select * from \(TableName.presetMaster) where \(TableField.id)=?"
            if ID == 0 {
                query = "select * from \(TableName.presetMaster)"
            } else {
                query = "select * from \(TableName.presetMaster) where \(TableField.id)=?"
            }
            
            do {
                let results = try database.executeQuery(query!, values: [ID])
                
                while results.next() {
                    singlePreset = Preset(id: Int(results.int(forColumn: TableField.id)), preset_name: results.string(forColumn: TableField.preset_name), bass_color: Int(results.int(forColumn: TableField.bass_color)), bass_bright: Int(results.int(forColumn: TableField.bass_bright)), bass_color_options: Int(results.int(forColumn: TableField.bass_color_options)), bass_animation_option: Int(results.int(forColumn: TableField.bass_animation_option)), bass_auto_animation: Int(results.int(forColumn: TableField.bass_auto_animation)), mid_color: Int(results.int(forColumn: TableField.mid_color)), mid_bright: Int(results.int(forColumn: TableField.mid_bright)), mid_color_options: Int(results.int(forColumn: TableField.mid_color_options)), mid_animation_option: Int(results.int(forColumn: TableField.mid_animation_option)), mid_auto_animation: Int(results.int(forColumn: TableField.mid_auto_animation)), trebel_color: Int(results.int(forColumn: TableField.trebel_color)), trebel_bright: Int(results.int(forColumn: TableField.trebel_bright)), trebel_color_options: Int(results.int(forColumn: TableField.trebel_color_options)), trebel_animation_option: Int(results.int(forColumn: TableField.trebel_animation_option)), trebel_auto_animation: Int(results.int(forColumn: TableField.trebel_auto_animation)))
                    arrData.add(singlePreset)
                }
            }
            catch {
                print(error.localizedDescription)
            }
            
            database.close()
        }
        completionHandler(arrData)
    }
    

    func executeQuery(strQuery: String, values: [Any])-> Bool {
        var flag = false

        if openDatabase() {
            do {
                try database.executeUpdate(strQuery, values: values)
                flag = true
            }
            catch {
                print(error.localizedDescription)
            }
            
            database.close()
        }
        return flag
    }
    
    
    func deleteAllPreset()-> Bool {
        var flag = false
        if openDatabase() {
            do {
                try database.executeUpdate("Delete from \(TableName.presetMaster)", values: nil)
                flag = true
            }
            catch {
                print(error.localizedDescription)
            }
            
            database.close()
        }
        return flag
    }
    
    func getAllPresetName()-> NSMutableArray {
        
        let arrNames = NSMutableArray()
        if openDatabase() {
            let query: String? = "select \(TableField.preset_name) from \(TableName.presetMaster)"
            do {
                let results = try database.executeQuery(query!, values: nil)
                while results.next() {
                    arrNames.add(results.string(forColumn: TableField.preset_name) ?? "")
                }
            }
            catch {
                print(error.localizedDescription)
            }
            database.close()
        }
        return arrNames
    }
    
    func nameAlreadyExist(strQuery: String, values: [Any])-> Bool {
        let arrNames = NSMutableArray()
        if openDatabase() {
            do {
                let results = try database.executeQuery(strQuery, values: values)
                if results.next() {
                    arrNames.add(results.string(forColumn: TableField.preset_name) ?? "")
                }
                else {
                    print(database.lastError())
                }
            }
            catch {
                print(error.localizedDescription)
            }
            database.close()
        }
        if arrNames.count > 0 {
            return true
        } else {
            return false
        }
    }
    
}
