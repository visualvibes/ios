//
//  AppPrefsManager.swift
//  PastZero
//
//  Created by Sunil Zalavadiya on 7/28/16.
//  Copyright © 2016 Sunil Zalavadiya. All rights reserved.
//

import UIKit
import CoreLocation
// swiftlint:disable all
class AppPrefsManager: NSObject {

    static let sharedInstance = AppPrefsManager()
    let TOKEN = "token"
    let EMAIL = "email"
    let USERBADGES = "USERBADGES"
    let ALLBADGES = "ALLBADGES"
    let USERDATA = "USERDATA"
    let USERALLDATA = "USERALLDATA"
    let SELECTEDPRESET = "SELECTEDPRESET"
    let MAINPRESET = "MAINPRESET"
    let VALUECHANGE = "VALUECHANGE"
    
    let APNSTOKEN = "APNSToken"
    let JWTTOKEN = "jwttoken"
    let SETCOUNT = "SETCOUNT"
    let USERID = "USERID"
    let CURRENTLAT = "CURRENTLAT"
    let CURRENTLONG = "CURRENTLONG"

    func setDataToPreference(data: AnyObject, forKey key: String) {
        do {
            var archivedData = Data()
            if #available(iOS 11.0, *) {
                archivedData = try NSKeyedArchiver.archivedData(withRootObject: data, requiringSecureCoding: true)
            } else {
                archivedData = NSKeyedArchiver.archivedData(withRootObject: data)
            }
            UserDefaults.standard.set(archivedData, forKey: key)
            UserDefaults.standard.synchronize()
        }
        catch {
            print("Unexpected error: \(error).")
        }
    }
    
    func getDataFromPreference(key: String) -> AnyObject? {
        let archivedData = UserDefaults.standard.object(forKey: key)
        if(archivedData != nil) {
            do {
                var unArchivedData: Any?
                if #available(iOS 11.0, *) {
                    unArchivedData =  try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(archivedData as! Data) as AnyObject
                } else {
                    unArchivedData = NSKeyedUnarchiver.unarchiveObject(with: archivedData as! Data) as AnyObject
                }
                return unArchivedData as AnyObject
            } catch {
                print("Unexpected error: \(error).")
            }
        }
        return nil
    }
    
    func removeDataFromPreference(key: String) {
        UserDefaults.standard.removeObject(forKey: key)
        UserDefaults.standard.synchronize()
    }
    
    func isKeyExistInPreference(key: String) -> Bool {
        if(UserDefaults.standard.object(forKey: key) == nil) {
            return false
        }
        return true
    }
    
    func setCount(obj: AnyObject) {
        setDataToPreference(data: obj as AnyObject, forKey: SETCOUNT)
    }
    
    func getCount() -> Int? {
        let count = getDataFromPreference(key: SETCOUNT)
        return count as? Int
    }
    
    func setAPNsToken(obj: AnyObject) {
        setDataToPreference(data: obj as AnyObject, forKey: APNSTOKEN)
    }

    func getAPNsToken() -> String? {
        let strToken = getDataFromPreference(key: APNSTOKEN)
        return strToken as? String
    }

    func setUserId(obj: Int) {
        setDataToPreference(data: obj as AnyObject, forKey: USERID)
    }
    
    func getUserId() -> Int? {
        let count = getDataFromPreference(key: USERID)
        return count as? Int
    }
    
    func setJwtToken(obj: AnyObject?) {
        setDataToPreference(data: obj as AnyObject, forKey: JWTTOKEN)
    }
    
    func getJwtToken() -> String? {
        let strToken = getDataFromPreference(key: JWTTOKEN)
        return strToken as? String
    }
    
    func setCurrentLat(obj: AnyObject?) {
        setDataToPreference(data: obj as AnyObject, forKey: CURRENTLAT)
    }
    
    func getCurrentLat() -> Double? {
        let lat = getDataFromPreference(key: CURRENTLAT)
        return lat as? Double
    }
    
    func setCurrentLong(obj: AnyObject?) {
        setDataToPreference(data: obj as AnyObject, forKey: CURRENTLONG)
    }
    
    func getCurrentLong() -> Double? {
        let long = getDataFromPreference(key: CURRENTLONG)
        return long as? Double
    }
    
    // Token
    func setToken(obj: AnyObject?) {
        setDataToPreference(data: obj as AnyObject, forKey: TOKEN)
    }
    
    func getToken() -> String? {
        let token = getDataFromPreference(key: TOKEN)
        return token as? String
    }
    
    // User Email
    func setEmail(obj: AnyObject?) {
        setDataToPreference(data: obj as AnyObject, forKey: EMAIL)
    }
    
    func getEmail() -> String? {
        let token = getDataFromPreference(key: EMAIL)
        return token as? String
    }
    
    
    // User Badge Info
    func setUserBadges(obj: AnyObject?) {
        setDataToPreference(data: obj as AnyObject, forKey: USERBADGES)
    }
    
    func getUserBadges() -> NSDictionary? {
        let token = getDataFromPreference(key: USERBADGES)
        return token as? NSDictionary
    }
    
    // All Badge Info
    func setAllBadges(obj: AnyObject?) {
        setDataToPreference(data: obj as AnyObject, forKey: ALLBADGES)
    }
    
    func getAllBadges() -> NSDictionary? {
        let token = getDataFromPreference(key: ALLBADGES)
        return token as? NSDictionary
    }
    
    // User info
    func setUserAllData(obj: AnyObject?) {
        setDataToPreference(data: obj as AnyObject, forKey: USERALLDATA)
    }
    
    func getUserAllData() -> NSDictionary? {
        let allData = getDataFromPreference(key: USERALLDATA)
        return allData as? NSDictionary
    }
    
    //Selected Preset
    func setSelectedPreset(obj: AnyObject) {
        setDataToPreference(data: obj as AnyObject, forKey: SELECTEDPRESET)
    }
    
    func getSelectedPreset() -> Preset? {
        do {
            if self.isKeyExistInPreference(key: SELECTEDPRESET) {
                let userData = getDataFromPreference(key: SELECTEDPRESET) as! NSDictionary
                return try JSONDecoder().decode(Preset.self, from: (userData.dataReturn(isParseDirect: true))!)
            }
            return nil
        }
        catch let err {
            print("Err", err)
            return nil
        }
    }
    
    
    //main preset
    func setMainPreset(obj: AnyObject) {
        setDataToPreference(data: obj as AnyObject, forKey: MAINPRESET)
    }
    
    func getMainPreset() -> Preset? {
        do {
            if self.isKeyExistInPreference(key: MAINPRESET) {
                let userData = getDataFromPreference(key: MAINPRESET) as! NSDictionary
                return try JSONDecoder().decode(Preset.self, from: (userData.dataReturn(isParseDirect: true))!)
            }
            return nil
        }
        catch let err {
            print("Err", err)
            return nil
        }
    }
    
    
    //value change
    func setValueChange(obj: Bool) {
        setDataToPreference(data: obj as AnyObject, forKey: VALUECHANGE)
    }
    
    func getValueChange() -> Bool? {
            let isChange = getDataFromPreference(key: VALUECHANGE)
            return isChange as? Bool
    }
    
}
