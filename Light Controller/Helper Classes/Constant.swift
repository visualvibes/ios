//
//  Constant.swift
//  GreekToMe
//
//  Created by Darshan Gajera on 5/14/18.
//  Copyright © 2018 TechARK. All rights reserved.
//
// swiftlint:disable all
import UIKit
struct GlobalConstants {
    static let APPNAME = "Visual Vibes"
}

enum Storyboard: String {
    case main    = "Main"
    func storyboard() -> UIStoryboard {
        return UIStoryboard(name: self.rawValue, bundle: Bundle.main)
    }
}

enum Color: String {
    case SliderSelction  = "0080ff"
    case SliderDefault  = "0048b1"
    
    case SliderTextSelection  = "ffffff"
    case SliderTextDefault  = "7887b5"
    
    case AppColorCode  = "F9F9F9"
    
    func color() -> UIColor {
        return UIColor.hexStringToUIColor(hex: self.rawValue)
    }
}

enum Identifier: String {
    // Main Storyboard
    case Preset = "PresetVC"
    case AddPreset = "AddPresetVC"
    
    
    
}

struct  TableName {
    static let presetMaster = "tbl_preset_master"
}

struct TableField {
    static let id = "id"
    static let preset_name = "preset_name"
    
    static let bass_color = "bass_color"
    static let bass_bright = "bass_bright"
    static let bass_color_options = "bass_color_options"
    static let bass_animation_option = "bass_animation_option"
    static let bass_auto_animation = "bass_auto_animation"
    
    static let mid_color = "mid_color"
    static let mid_bright = "mid_bright"
    static let mid_color_options = "mid_color_options"
    static let mid_animation_option = "mid_animation_option"
    static let mid_auto_animation = "mid_auto_animation"
    
    static let trebel_color = "trebel_color"
    static let trebel_bright = "trebel_bright"
    static let trebel_color_options = "trebel_color_options"
    static let trebel_animation_option = "trebel_animation_option"
    static let trebel_auto_animation = "trebel_auto_animation"
}

struct ErrorMesssages {
    static let wrong = "Something went wrong!"
    static let nameExist = "Preset name already exist!"
    static let validDevice = "Please choose valid device"
    static let enterPresetName = "Please enter preset name"
}

struct SuccessMesssages {
    static let rename = "Preset renamed successfully."
    static let update = "Preset updated successfully."
    static let copy = "Preset copied successfully."
    static let save = "Preset saved successfully."
    static let delete = "Preset deleted successfully."
    
}


