//
//  LedSetupController.swift
//  Light Controller
//
//  Created by Josh Jordan on 6/4/17.
//  Copyright © 2017 Josh Jordan. All rights reserved.
//

import UIKit

class LedSetupController: UIViewController {
    var tabbar:BleTabBarController!

    @IBOutlet weak var filterLabel: UILabel!
    @IBOutlet weak var trebleCountLabel: UILabel!
    @IBOutlet weak var bassCountLabel: UILabel!
    @IBOutlet weak var midCountLabel: UILabel!
    
    @IBOutlet weak var filterSlider: UISlider!
    @IBOutlet weak var trebleCountSlider: UISlider!
    @IBOutlet weak var midCountSlider: UISlider!
    @IBOutlet weak var bassCountSlider: UISlider!
 
    override func viewDidLoad() {
        super.viewDidLoad()
        tabbar = self.tabBarController as? BleTabBarController
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        print("selected index: \(tabbar.selectedIndex)")
        filterLabel.text = "Filter Level \(tabbar.filter)"
        filterSlider.setValue(Float(tabbar.filter), animated: false)
        trebleCountSlider.setValue(Float(tabbar.trebleCount), animated: false)
        trebleCountLabel.text = "Treble Light Count \(tabbar.trebleCount)"
        midCountSlider.setValue(Float(tabbar.midCount), animated: false)
        midCountLabel.text = "Mid Light Count \(tabbar.midCount)"
        bassCountSlider.setValue(Float(tabbar.bassCount), animated: false)
        bassCountLabel.text = "Bass Light Count \(tabbar.bassCount)"
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func filterChanged(_ sender: UISlider) {
        sender.setValue(sender.value.rounded(.down), animated: true)
        tabbar.filter = Int(sender.value)
        print("filter level: \(tabbar.filter)")
        filterLabel.text = "Filter Level \(UInt8(sender.value))"
        tabbar.bleFifo.append((3, Data([9, 0x00, UInt8(sender.value)])))
        tabbar.nextBleMessage()
    }

    @IBAction func trebleCountFinish(_ sender: UISlider) {
        print("treble count finished \(Int(sender.value))")
        tabbar.trebleCount = Int(sender.value)
        tabbar.bleFifo.append((3, Data([39, UInt8(UInt16(sender.value) >> 8), UInt8(UInt16(sender.value) & 0xff)])))
        tabbar.nextBleMessage()
    }
    
    @IBAction func trebleCountChanged(_ sender: UISlider) {
        sender.setValue(sender.value.rounded(.down), animated: true)
        //print("treble count: \(Int(sender.value))")
        trebleCountLabel.text = "Treble Light Count \(Int(sender.value))"
    }
    
    @IBAction func trebleButtonValueChanged(_ sender: UIButton) {
        print(trebleCountSlider.value)
        if sender.titleLabel?.text == "-" {
            // -
            trebleCountSlider.value = trebleCountSlider.value - 1.0
        } else {
             // +
            trebleCountSlider.value = trebleCountSlider.value + 1.0
        }
        trebleCountLabel.text = "Treble Light Count \(Int(trebleCountSlider.value))"
        tabbar.trebleCount = Int(trebleCountSlider.value)
        tabbar.bleFifo.append((3, Data([39, UInt8(UInt16(trebleCountSlider.value) >> 8), UInt8(UInt16(trebleCountSlider.value) & 0xff)])))
        tabbar.nextBleMessage()
    }
    
    @IBAction func midCountFinish(_ sender: UISlider) {
        print("mid count finished \(Int(sender.value))")
        tabbar.midCount = Int(sender.value)
        tabbar.bleFifo.append((3, Data([29, UInt8(UInt16(sender.value) >> 8), UInt8(UInt16(sender.value) & 0xff)])))
        tabbar.nextBleMessage()
    }
    
    @IBAction func midCountChanged(_ sender: UISlider) {
        sender.setValue(sender.value.rounded(.down), animated: true)
        //print("mid count: \(Int(sender.value))")
        midCountLabel.text = "Mid Light Count \(Int(sender.value))"
    }
    
    @IBAction func midButtonValueChanged(_ sender: UIButton) {
        if sender.titleLabel?.text == "-" {
            // -
            midCountSlider.value = midCountSlider.value - 1.0
        } else {
            // +
            midCountSlider.value = midCountSlider.value + 1.0
        }
        midCountLabel.text = "Mid Light Count \(Int(midCountSlider.value))"
        tabbar.midCount = Int(midCountSlider.value)
        tabbar.bleFifo.append((3, Data([29, UInt8(UInt16(midCountSlider.value) >> 8), UInt8(UInt16(midCountSlider.value) & 0xff)])))
        tabbar.nextBleMessage()
    }

    
    @IBAction func bassCountFinish(_ sender: UISlider) {
        print("bass count finished \(Int(sender.value))")
        tabbar.bassCount = Int(sender.value)
        tabbar.bleFifo.append((3, Data([19, UInt8(UInt16(sender.value) >> 8), UInt8(UInt16(sender.value) & 0xff)])))
        tabbar.nextBleMessage()
    }
    
    @IBAction func bassCountChanged(_ sender: UISlider) {
        sender.setValue(sender.value.rounded(.down), animated: true)
        //print("bass count: \(Int(sender.value))")
        bassCountLabel.text = "Bass Light Count \(Int(sender.value))"
    }
    

    @IBAction func bassButtonValueChanged(_ sender: UIButton) {
        if sender.titleLabel?.text == "-" {
            // -
            bassCountSlider.value = bassCountSlider.value - 1.0
        } else {
            // +
            bassCountSlider.value = bassCountSlider.value + 1.0
        }
        bassCountLabel.text = "Bass Light Count \(Int(bassCountSlider.value))"
        tabbar.bassCount = Int(bassCountSlider.value)
        tabbar.bleFifo.append((3, Data([19, UInt8(UInt16(bassCountSlider.value) >> 8), UInt8(UInt16(bassCountSlider.value) & 0xff)])))
        tabbar.nextBleMessage()
    }
    
    @IBAction func bassProtocolChanged(_ sender: UISegmentedControl) {
        print("bass protocol changed \(sender.selectedSegmentIndex)")
    }
}
